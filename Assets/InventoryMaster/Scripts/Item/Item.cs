﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Item
{
    public string itemName;                                     //itemName of the item
    public int itemID;                                          //itemID of the item
    public string itemDesc;                                     //itemDesc of the item
    public Sprite itemIcon;                                     //itemIcon of the item
    public GameObject itemModel;                                //itemModel of the item
    public int itemValue = 1;                                   //itemValue is at start 1
    public ItemType itemType;                                   //itemType of the Item
    public float itemWeight;                                    //itemWeight of the item
    public int maxStack = 1;
    public int indexItemInList = 999;    
    public int rarity;

    [SerializeField]
    public List<ItemAttribute> itemAttributes = new List<ItemAttribute>();    
    
    public Item(){}

    public Item(string name, int id, string desc, Sprite icon, GameObject model, int maxStack, ItemType type, string sendmessagetext, List<ItemAttribute> itemAttributes)                 //function to create a instance of the Item
    {
        itemName = name;
        itemID = id;
        itemDesc = desc;
        itemIcon = icon;
        itemModel = model;
        itemType = type;
        this.maxStack = maxStack;
        this.itemAttributes = itemAttributes;
    }

    public Item getCopy()
    {
        return (Item)this.MemberwiseClone();        
    }

	// @SurvivorsTeam
	/// <summary>
	/// Overrides the base Equals() method to verify if an 
	/// <see cref="Item"/> ID or name is equal to another.
	/// </summary>
	/// <param name="obj">The <see cref="System.Object"/> to compare with the current <see cref="Item"/>.</param>
	/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current <see cref="Item"/>; otherwise, <c>false</c>.</returns>
	public override bool Equals (object obj)
	{
		bool isEquals = false;
		var otherItem = (Item) obj;

		if (otherItem.itemID.Equals (this.itemID)) {
			isEquals = true;
		} else if (otherItem.itemName != null && otherItem.itemName.Equals (this.itemName)) {
			isEquals = true;
		} else {

			isEquals = base.Equals (obj);
		}

		return isEquals;

	}

	// @SurvivorsTeam
	/// <summary>
	/// Serves as a hash function for a <see cref="Item"/> object.
	/// </summary>
	/// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a hash table.</returns>
	public override int GetHashCode ()
	{
		return this.itemID.GetHashCode ();
	}
    
    
}


