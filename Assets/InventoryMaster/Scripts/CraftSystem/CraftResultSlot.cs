﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CraftResultSlot : MonoBehaviour
{

    CraftSystem craftSystem;
    public int temp = 0;
    GameObject itemGameObject;

    protected Inventory mainInventory;


    // Use this for initialization
    void Start()
    {
        craftSystem = transform.parent.GetComponent<CraftSystem>();

        itemGameObject = (GameObject)Instantiate(Resources.Load("Prefabs/Item") as GameObject);
        itemGameObject.transform.SetParent(this.gameObject.transform);
        itemGameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
        itemGameObject.GetComponent<DragItem>().enabled = false;
        itemGameObject.SetActive(false);
        itemGameObject.transform.GetChild(1).GetComponent<Text>().enabled = true;

		// @SurvivorsTeam
		mainInventory = GetMainInventory ();
		if (mainInventory != null) {
			
			itemGameObject.transform.GetChild(1).GetComponent<RectTransform>().localPosition = new Vector2(mainInventory.positionNumberX, mainInventory.positionNumberY);
		}

    }

    // Update is called once per frame
    void Update()
    {
        if (craftSystem.possibleItems.Count != 0)
        {
            itemGameObject.GetComponent<ItemOnObject>().item = craftSystem.possibleItems[temp];
            itemGameObject.SetActive(true);
        }
        else
            itemGameObject.SetActive(false);

    }

	// @SurvivorsTeam
	/// <summary>
	/// Gets the MainInventory component related with
	/// the parent of the CraftSystem component.
	/// </summary>
	/// <returns>The MainInventory component script.</returns>
	public virtual Inventory GetMainInventory()
	{
		var inventories = transform.parent.parent.GetComponentsInChildren<Inventory> ();
		Inventory main = null;

		foreach (var inv in inventories) {
			
			if (inv.mainInventory) {
				main = inv;
				break;
			}
		}

		return main;
	}

}
