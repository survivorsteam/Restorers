﻿using UnityEngine;
using System;

namespace PlatformerPro.Extensions {
	
	/// <summary>
	/// The camera struct with some inspector fields
	/// </summary>
	[Serializable]
	public struct CameraRef {

		/// <summary>
		/// The camera game object reference.
		/// </summary>
		[SerializeField]
		[Tooltip("The camera game object reference")]
		private GameObject gameObject;

		/// <summary>
		/// The camera destination coordinates
		/// </summary>
		[SerializeField]
		[Tooltip("The camera destination coordinates")]
		private Vector3 destination;

		/// <summary>
		/// <para>
		/// The camera script reference to focus on moved player.
		/// </para>
		/// Needs be a class that implements<see cref="ICameraMove.Zoom()"/>interface method
		/// </summary>
		/// <typeparam>
		private ICameraMove moveScript;

		/// <summary>
		/// Property to gets or sets the gameObject field
		/// </summary>
		/// <value>The unity GameObject reference</value>
		public GameObject Object {
			get	{
				return gameObject;
			}

			set	{
				gameObject = value;
			}
		}

		/// <summary>
		/// Property to gets or sets the destination coordinates
		/// </summary>
		/// <value>The Vector3 destination</value>
		public Vector3 Destination {
			get	{
				return destination;
			}

			set	{
				destination = value;
			}
		}

		/// <summary>
		/// Property to gets or sets the component script 
		/// that implements <see cref="ICameraMove"/> interface.
		/// </summary>
		/// <value>The move script component.</value>
		public ICameraMove MoveScript {
			get	{
				return moveScript;
			}

			set	{
				moveScript = value;
			}
		}

		/// <summary>
		/// <para>
		/// Init the some fields of the camera. This method works similar a Constructor
		/// </para>
		/// Use this to initialize field values on <see cref="UnityEngine.MonoBehaviour.Awake()"/>hook
		/// </summary>
		/// <param name="player">
		/// A <see cref="PlayerRef"/> struct instance to get their coordinates
		/// </param>
		public void Init(PlayerRef player){
			moveScript = moveScript ?? gameObject.GetComponent<ICameraMove>();
			
			if (destination.Equals (Vector3.zero)) {
				destination = new Vector3 (Object.transform.position.x, player.Destination.y, 0.8f);
			}
		}

	}
}