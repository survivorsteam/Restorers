﻿using UnityEngine;
using System;

namespace PlatformerPro.Extensions {
	
	/// <summary>
	/// The player struct with some inspector fields
	/// </summary>
	[Serializable]
	public struct PlayerRef {

		/// <summary>
		/// The player transform reference to move
		/// </summary>
		[SerializeField]
		[Tooltip("The player transform to change for a new position")]
		private Transform transform;

		/// <summary>
		/// The tag to check if player collides 
		/// with this GameObject
		/// </summary>
		[SerializeField]
		[Tooltip("The tag to check if player collides with this GameObject")]
		private string tag;

		/// <summary>
		/// The new destination to move the player
		/// </summary>
		[SerializeField]
		[Tooltip("The new destination to move the player")]
		private Vector3 destination;

		/// <summary>
		/// Property to gets or sets the tag field.
		/// </summary>
		/// <value>The string tag. Default: "Player"</value>
		public string Tag {
			get	{
				return tag;
			}

			set	{
				tag = value.Length > 0 ? value : "Player";
			}
		}

		/// <summary>
		/// Property to gets or sets the transform reference.
		/// </summary>
		/// <value>The transform reference.</value>
		public Transform Transform {
			get {
				return transform;
			}

			set {
				transform = value;
			}
		}

		/// <summary>
		/// Property to gets or sets the destination.
		/// </summary>
		/// <value>The Vector3 destination.</value>
		public Vector3 Destination {
			get {
				return destination.magnitude > 0.0f ? destination : Vector3.zero;
			}

			set {
				destination = value;
			}
		}

	}
}

