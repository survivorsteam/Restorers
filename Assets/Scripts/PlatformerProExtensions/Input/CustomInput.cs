﻿using UnityEngine;
using System.Collections;

namespace PlatformerPro.Extensions {

	public class CustomInput : StandardInput {

		/// <summary>
		/// A "digital" version of the horizontal axis in which the only valid values are -1 for LEFT, 
		/// 0 for NONE, and 1 for RIGHT.
		/// </summary>
		override public int HorizontalAxisDigital
		{
			get
			{
				// Controller
				if (enableController)
				{
					if (horizontalAxisName != null && horizontalAxisName != "" && horizontalAxisName != "none")
					{
						float axisValue = UnityEngine.Input.GetAxis(horizontalAxisName);
						if (axisValue > digitalHorizontalThreshold) return reverseHorizontalAxis ? -1 : 1;
						if (axisValue < -digitalHorizontalThreshold) return reverseHorizontalAxis ? 1 : -1;
					}
				}
				if (enableKeyboard)
				{
					// Both down return 0
					if (UnityEngine.Input.GetKey(right) && UnityEngine.Input.GetKey(left)) return 0;
					// Right
					if (UnityEngine.Input.GetKey (right)) {
						int value = reverseHorizontalAxis ? -1 : 1;
						return value;
					}
					// Left
					if (UnityEngine.Input.GetKey (left)) {
						return reverseHorizontalAxis ? 1 : -1;
					}
				}
				// Otherwise 0
				return 0;
			}
		}
	}
}
