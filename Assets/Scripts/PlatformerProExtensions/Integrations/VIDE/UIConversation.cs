﻿using UnityEngine;
using System.Collections;

namespace PlatformerPro.Extensions.Integrations {

	public class UIConversation : VIDE_UIConversation {

		public override bool CollectItem(ItemComment item, GameObject receiver) {
			
			var itemManager = receiver.GetComponentInChildren<ItemManager> ();
			var itemClass = item.itemClass == "STACKABLE" ? ItemClass.STACKABLE : ItemClass.SINGLE;

			int result = itemManager.CollectItem (itemClass, item.name, 1);

			return result != 0;
		}

	}
}
