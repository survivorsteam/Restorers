﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using PlatformerPro.Extras;

namespace PlatformerPro.Extensions.Integrations.FMODSfx {


	public class UIMenuItemSfx : UIMenuItem {

		[FMODUnity.EventRef]
		public string interactEvent;

		protected Button btn;

		protected bool playOnClick = false;

		public virtual void Awake() {

			btn = GetComponent<Button> ();
			if (btn != null && interactEvent.Length > 0) {
				btn.onClick.AddListener (PlayClick);
				playOnClick = true;
			} 
		}

		public virtual void PlayClick() {

			FMODUnity.RuntimeManager.PlayOneShot (interactEvent);
		}

		public override void DoAction() {
			
			if (interactEvent.Length > 0 && !playOnClick) {
				PlayClick ();
			}

			base.DoAction ();
		}
	}
}
