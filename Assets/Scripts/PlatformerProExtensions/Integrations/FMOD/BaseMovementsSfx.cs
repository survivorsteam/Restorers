﻿using UnityEngine;
using System.Collections;

namespace PlatformerPro.Extensions.Integrations.FMODSfx {

	public class BaseMovementsSfx : MonoBehaviour {

		[FMODUnity.EventRef]
		public string FallEvent;

		[FMODUnity.EventRef]
		public string AttackEvent;

		[FMODUnity.EventRef]
		public string DeathEvent;

		[FMODUnity.EventRef]
		public string HurtEvent;

		/// <summary>
		/// Cached Character/Enemy component reference
		/// </summary>
		protected IMob characterOrEnemy;

		public virtual void Awake() {

			characterOrEnemy = (IMob) GetComponentInParent(typeof(IMob));

			if (characterOrEnemy == null) {
				Debug.LogError ("A Character or Enemy with Animation States is required for play sounds");
			}

		}

		public virtual void PlayAttack() {
			
			if (AttackEvent.Length > 0) {

				if (characterOrEnemy.AnimationState == AnimationState.ATTACK_STAB
				    || characterOrEnemy.AnimationState == AnimationState.ATTACK) {

					FMODUnity.RuntimeManager.PlayOneShot (AttackEvent);
				}
			}
		}

		public virtual void PlayHurt() {

			if (HurtEvent.Length > 0) {
				
				if (characterOrEnemy.AnimationState == AnimationState.HURT_NORMAL) {
					
					FMODUnity.RuntimeManager.PlayOneShot (HurtEvent);
				}
			}

		}

		public virtual void PlayDeath() {

			if (DeathEvent.Length > 0) {
				
				if (characterOrEnemy.AnimationState == AnimationState.DEATH) {
					
					FMODUnity.RuntimeManager.PlayOneShot (DeathEvent);
				}
			}
			
		}

	}
}
