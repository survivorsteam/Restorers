﻿using UnityEngine;
using System.Collections;

namespace PlatformerPro.Extensions.Integrations.FMODSfx {

	public class InventorySfx : MonoBehaviour {

		[FMODUnity.EventRef]
		public string itemEvent;

		[SerializeField]
		protected string FMODParameter = "Action";

		public virtual void OnEnable() {

			DragItem.dragEndSuccess += HandleDragItemEnd;
			ConsumeItem.craftCreate += HandleCraftCreate;

		}

		public virtual void OnDisable() {

			DragItem.dragEndSuccess -= HandleDragItemEnd;
			ConsumeItem.craftCreate -= HandleCraftCreate;
		}

		public virtual void HandleDragItemEnd(GameObject itemObj) {

			PlayItemAction (ItemAction.DRAG_END);
		}

		public virtual void HandleCraftCreate(global::Item itemObj) {

			PlayItemAction (ItemAction.CRAFT_CREATE);
		}

		public virtual void PlayItemAction(ItemAction action) {

			if (itemEvent.Length > 0) {

				var itemEventInstance = FMODUnity.RuntimeManager.CreateInstance (itemEvent);

				itemEventInstance.setParameterValue (FMODParameter, (float) action);

				itemEventInstance.start ();
				itemEventInstance.release ();
			}
		}
	}

	public enum ItemAction
	{
		DRAG_END,
		CRAFT_CREATE
	}
}
