﻿using UnityEngine;
using System.Collections;
using PlatformerPro;

namespace PlatformerPro.Extensions.Integrations.FMODSfx {

	public class DoorSfx : MonoBehaviour {

		[FMODUnity.EventRef]
		public string TryOpenEvent;

		[FMODUnity.EventRef]
		public string OpeningEvent;

		[FMODUnity.EventRef]
		public string CloseEvent = null;


		protected EnterableDoor door;

		public virtual void Awake() {
			
			door = GetComponent<EnterableDoor> ();
			if (door == null) {

				Debug.LogError ("A EnterableDoor component is required to play sound effects");
			}
		}

		public virtual void OnEnable() {


			if (CloseEvent.Length > 0) {
				
				door.Entered += HandleClosed;
			} else if(OpeningEvent.Length > 0) {

				door.Entered += HandleEntered;
			}

		}

		public virtual void OnDisable() {

			if (CloseEvent.Length > 0) {

				door.Entered -= HandleClosed;
			} else if(OpeningEvent.Length > 0)  {

				door.Entered -= HandleEntered;
			}

		}

		public virtual void PlayOpen() {

			if (TryOpenEvent != null) {

				FMODUnity.RuntimeManager.PlayOneShot (TryOpenEvent);
			}
		}

		public virtual void PlayEntering() {

			if (OpeningEvent != null) {

				FMODUnity.RuntimeManager.PlayOneShot (OpeningEvent);
			}
		}

		public virtual void PlayClose() {

			if (CloseEvent.Length > 0) {

				FMODUnity.RuntimeManager.PlayOneShot (CloseEvent);
			}
		}

		protected void HandleEntered(object sender, DoorEventArgs e) {
			
			PlayEntering ();
		}

		protected void HandleClosed(object sender, DoorEventArgs e) {

			PlayClose ();
		}


	}
}
