﻿using UnityEngine;
using System.Collections;
using PlatformerPro;

namespace PlatformerPro.Extensions.Integrations.FMODSfx {

	public class EnemyMovementsSfx : BaseMovementsSfx {

		[FMODUnity.EventRef]
		public string trackEvent;

		[SerializeField]
		protected string phaseToTrack;

		[SerializeField]
		protected string FMODParameter = "State";

		[System.NonSerialized]
		public static FMOD.Studio.EventInstance track;

		protected SequenceDrivenEnemy sequenceEnemy;

		protected Enemy enemy;


		public override void Awake() {
			
			base.Awake ();
			sequenceEnemy = GetComponentInParent<SequenceDrivenEnemy> ();
		}

		public virtual void OnEnable() {

			if (sequenceEnemy != null) {
				sequenceEnemy.PhaseEnter += HandlePhase;
				sequenceEnemy.Died += HandleDie;
			} else {

				enemy = GetComponentInParent<Enemy> ();
				if (enemy != null) {
					enemy.Died += HandleDie;
				}
			}
		}

		public virtual void OnDisable() {

			if (sequenceEnemy != null) {
				
				sequenceEnemy.PhaseEnter -= HandlePhase;
				sequenceEnemy.Died -= HandleDie;
			}

			if (enemy != null) {
				enemy.Died -= HandleDie;
			}

			if (track != null) {
				track.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
				track = null;
			}
		}

		public virtual void HandlePhase(object sender, PhaseEventArgs e) {
			
			if (e.PhaseName == phaseToTrack) {
				PlayTrack (TrackState.FIGHT);
			}
		}

		public virtual void HandleDie(object sender, DamageInfoEventArgs e) {

			PlayTrack (TrackState.THRILLER);
		}

		public virtual void PlayTrack(TrackState state) {

			if (trackEvent.Length > 0) {

				if (track == null) {
					var eventDesc = FMODUnity.RuntimeManager.GetEventDescription (trackEvent);
					FMOD.Studio.EventInstance[] trackList = new FMOD.Studio.EventInstance[0];

					if (eventDesc.getInstanceList(out trackList) == FMOD.RESULT.OK) {
						if (trackList.Length > 0) {
							track = trackList [0];
						}
					}

					if (track == null) {
						
						track = FMODUnity.RuntimeManager.CreateInstance (trackEvent);
						track.setParameterValue (FMODParameter, (float)state);
						track.start ();
						track.release ();
					} else {
						track.setParameterValue (FMODParameter, (float)state);
					}

				} else {

					track.setParameterValue (FMODParameter, (float) state);
				}
			}
		}
	}
}
