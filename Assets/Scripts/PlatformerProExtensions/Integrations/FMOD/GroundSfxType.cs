﻿using System;

namespace PlatformerPro.Extensions.Integrations.FMODSfx {

	public enum GroundSfxType {

		NONE = 0,
		EXTERNAL = 1,
		INTERNAL = 2
	}
}

