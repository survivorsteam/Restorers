﻿using UnityEngine;
using System.Collections.Generic;
using PlatformerPro;
using AnimationState = PlatformerPro.AnimationState;

namespace PlatformerPro.Extensions.Integrations.FMODSfx {
	
	public class CharacterMovementsSfx : BaseMovementsSfx {

		[FMODUnity.EventRef]
		public string FootstepsEvent;

		[FMODUnity.EventRef]
		public string MovePushableEvent;

		[SerializeField]
		protected string FMODParameter = "GroundType";

		/// <summary>
		/// Cached Character component reference
		/// </summary>
		protected Character character;

		/// <summary>
		/// Cached Character component reference
		/// </summary>
		protected Dictionary<string, GroundData> groundsToPlay = new Dictionary<string, GroundData>();

		/// <summary>
		/// Cached Tags reference
		/// </summary>
		protected Dictionary<string, GroundSfxType> tagsSound = new Dictionary<string, GroundSfxType>()
		{
			{"External", GroundSfxType.EXTERNAL},
			{"Internal", GroundSfxType.INTERNAL}
		};
																	

		/// <summary>
		/// Cached GroundData for current collided ground by character
		/// </summary>
		protected GroundData groundCurrent;

		protected FMOD.Studio.EventInstance movableEventInstance = null;

		protected bool playingMovable = false;


		#region unity hooks

		public override void Awake() {

			base.Awake ();

			if (!(characterOrEnemy is Character)) {
				
				Debug.LogError ("A Character sfx movements required a Character component");
			} else {

				character = (Character) characterOrEnemy;
			}
				
		}
			
		public virtual void Update() {

			if (character.Grounded && character.GroundCollider != null) {
				var platformCustom = character.GroundCollider.GetComponent<PlatformCustom> ();
				if (platformCustom == null && groundsToPlay.Count == 0) {

					GroundSfxType soundEffectType;
					if (tagsSound.TryGetValue (character.GroundCollider.tag, out soundEffectType)) {
						groundCurrent = new GroundData ();
						groundCurrent.type = soundEffectType;
					}
				} else {
					groundCurrent = new GroundData ();
					groundCurrent.ground = platformCustom;
					groundCurrent.type = platformCustom.soundType;
				}

				if (character.AnimationState == AnimationState.FALL) {

					if (groundCurrent != null) {
						FMODUnity.RuntimeManager.PlayOneShot (FallEvent);
					}
				}
			}

			if (character.AnimationState != AnimationState.PUSH
			    && character.AnimationState != AnimationState.PULL) {

				if (movableEventInstance != null && playingMovable) {
					movableEventInstance.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
					movableEventInstance.release ();

					movableEventInstance = null;
					playingMovable = false;
				}
			}
		}

		#endregion

		public virtual void PlaySteps() {

			if (character.AnimationState == AnimationState.WALK
				|| character.AnimationState == AnimationState.RUN) {

				if (groundCurrent != null) {
						
					var footsteps = FMODUnity.RuntimeManager.CreateInstance (FootstepsEvent);
					
					footsteps.setParameterValue (FMODParameter, (float) groundCurrent.type);
					
					footsteps.start ();
					footsteps.release ();
				} else {
					Debug.LogError ("A ground type to play specific FMOD sound was not found");
				}
			}

		}

		public virtual void PlayMovePushable() {


			if (character.AnimationState == AnimationState.PUSH
			    || character.AnimationState == AnimationState.PULL) {

				if (!playingMovable) {
					
					movableEventInstance = FMODUnity.RuntimeManager.CreateInstance (MovePushableEvent);
						
					movableEventInstance.start ();
					playingMovable = true;
				}
			}
		}
			
	}

	[System.Serializable]
	public class GroundData {

		public PlatformCustom ground;
		public GroundSfxType type;
	}

}
