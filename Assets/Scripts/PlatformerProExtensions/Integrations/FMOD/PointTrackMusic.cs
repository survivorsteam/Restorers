﻿using UnityEngine;
using System.Collections;

namespace PlatformerPro.Extensions.Integrations.FMODSfx {

	public class PointTrackMusic : MonoBehaviour {

		[FMODUnity.EventRef]
		public string trackEvent;

		[SerializeField]
		protected string FMODParameter = "State";

		[SerializeField]
		protected Enemy enemy;

		protected RespawnPoint respawnPoint;

		protected Trigger trigger;

		[System.NonSerialized]
		public static FMOD.Studio.EventInstance track;

		#region unity hooks

		public virtual void Awake() {
			
			respawnPoint = GetComponent<RespawnPoint> ();
			trigger = GetComponent<Trigger> ();

		}


		public virtual void OnEnable() {

			if (respawnPoint != null) {
				
				LevelManager.Instance.Respawned += HandleRespawn;
			}

			if (trigger != null) {

				trigger.TriggerEntered += HandleTriggerEnter;
			}

			if (enemy != null) {
				enemy.Died += HandleEnemyDie;
			}

		}
			

		public virtual void OnDisable() {

			if (respawnPoint != null) {

				LevelManager.Instance.Respawned -= HandleRespawn;
			}

			if (trigger != null) {

				trigger.TriggerEntered -= HandleTriggerEnter;
			}

			if (enemy != null) {
				enemy.Died -= HandleEnemyDie;
			}

			if (track != null) {
				track.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
				track = null;
			}
		}

		#endregion

		public virtual void PlayTrack(TrackState state) {

			if (trackEvent.Length > 0) {

				if (track == null) {
					
					track = FMODUnity.RuntimeManager.CreateInstance (trackEvent);
					track.setParameterValue (FMODParameter, (float) state);
					track.start ();
					track.release ();
				} else {

					track.setParameterValue (FMODParameter, (float) state);
				}
			}
		}

		public virtual void HandleRespawn(object sender, SceneEventArgs e) {
			
			if (respawnPoint != null && e.RespawnPoint == respawnPoint.identifier) {
				
				PlayTrack (TrackState.THRILLER);
			}
		}

		public virtual void HandleTriggerEnter(object sender, CharacterEventArgs e) {
			
			if (enemy != null) {

				if (enemy.State != EnemyState.DEAD) {
					
					PlayTrack (TrackState.FIGHT);
				}
			}
		}

		public virtual void HandleEnemyDie(object sender, DamageInfoEventArgs e) {
			
			PlayTrack (TrackState.THRILLER);
		}
	}

	public enum TrackState
	{
		THRILLER = 0,
		FIGHT = 1
	}
}
