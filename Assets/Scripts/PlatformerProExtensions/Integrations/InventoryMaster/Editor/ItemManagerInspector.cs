﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using PlatformerPro;

namespace PlatformerPro.Extensions.Integrations {
	
	[CustomEditor(typeof(ItemManager))]
	public class ItemManagerInspector : Editor {

		public virtual void OnEnable() {

			IM_Manager.ItemChanged += OnItemDatabase;
			IM_Manager.ItemRemoved += OnItemDatabaseRemove;
		}

		public virtual void OnDisable() {
			
			IM_Manager.ItemChanged -= OnItemDatabase;
			IM_Manager.ItemRemoved -= OnItemDatabaseRemove;
		}

		//TODO: Review this implementation do ADD/UPDATE InventoryMaster database
		public virtual void OnItemDatabase(global::Item item, string oldName = null) {
				
				var itemManager = (ItemManager) target;

				int existIndex = itemManager.collectibleItems.FindIndex (x => x.type == oldName || x.type == item.itemName);
				
				if (existIndex == -1) {
					
					var stackableItem = new StackableItemData ();
					stackableItem.type = item.itemName;
					stackableItem.max = item.maxStack;
					stackableItem.startingCount = 0;
					
					itemManager.collectibleItems.Add (stackableItem);
				} else {
					
					itemManager.collectibleItems[existIndex].type = item.itemName;
					itemManager.collectibleItems[existIndex].max = item.maxStack;
				}
		}

		public virtual void OnItemDatabaseRemove(global::Item item, string oldName = null) {

			var itemManager = (ItemManager) target;

			int existIndex = itemManager.collectibleItems.FindIndex (x => x.type == oldName || x.type == item.itemName);

			if (existIndex != -1) {
				itemManager.collectibleItems.RemoveAt (existIndex);
			}
		}

	}
}
