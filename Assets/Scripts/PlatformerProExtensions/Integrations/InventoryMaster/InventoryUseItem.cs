﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PlatformerPro.Extensions.Integrations {
	
	public class InventoryUseItem : MonoBehaviour {

		//TODO Add a Serialize object with more data about animation(e.g. time lenght)
		[SerializeField]
		protected WeaponAnimationData weaponAnimation;

		[SerializeField]
		protected MelleeAnimationData melleeAnimation;

		/// <summary>
		/// Cached equiped items to verify when a item is unequipped
		/// </summary>
		protected Dictionary<string, ItemAttribute> equipedItems = new Dictionary<string, ItemAttribute>();

		/// <summary>
		/// Cached BasicAttacks PlatformerPro component
		/// </summary>
		protected BasicAttacks basicAttacks;

		/// <summary>
		/// Cached CharacterHealth PlatformerPro component
		/// </summary>
		protected CharacterHealth health;

		/// <summary>
		/// Cached ItemManager component
		/// </summary>
		protected ItemManager itemManager;

		/// <summary>
		/// The cached added item from "ItemManager.ItemCollected" event
		/// </summary>
		protected GameObject addedItem = null;

		/// <summary>
		/// The cached Inventory database
		/// </summary>
		protected ItemDataBaseList inventoryItemList;


		protected Character character;

		public virtual void Awake() {
			
			itemManager = GetComponentInChildren<ItemManager> ();
			character = GetComponentInChildren<Character> ();
		}

		public virtual void OnEnable() {
			
			Inventory.ItemEquip += OnItemEquip;
			Inventory.UnEquipItem += OnItemUnEquip;

			Inventory.ItemConsumed += OnItemUse;

			if (itemManager != null) {
				itemManager.ItemCollected += OnItemManagerCollect;
				itemManager.ItemConsumed += OnItemManagerConsume;
			}
		}

		public virtual void OnDisable() {

			Inventory.ItemEquip -= OnItemEquip;
			Inventory.UnEquipItem -= OnItemEquip;

			Inventory.ItemConsumed -= OnItemUse;

			if (itemManager != null) {
				itemManager.ItemCollected -= OnItemManagerCollect;
				itemManager.ItemConsumed -= OnItemManagerConsume;
			}
		}

		protected virtual void OnItemManagerCollect(object sender, ItemEventArgs e) {
			
			ItemDataBaseList inventoryItemList = (ItemDataBaseList)Resources.Load("ItemDatabase");

			var item = inventoryItemList.getItemByName (e.Type);
			if (item == null) {
				Debug.LogErrorFormat ("The item {0} was not found on ItemDatabase", new string[]{ e.Type });
				return;
			}

			var playerInventory = GetComponent<PlayerInventory> ();

			if (playerInventory != null) {

				if (playerInventory.inventory != null) {
					
					var inventory = playerInventory.inventory.GetComponent<Inventory> ();

					if (inventory == null) {
						Debug.LogError ("The 'Inventory' component to addItemToInventory was not found");
						return;
					}
					
					addedItem = inventory.addItemToInventory (item.itemID, 1);

					inventory.OnUpdateItemList();
					inventory.stackableSettings ();
				} else {
					Debug.LogError ("The inventory gameObject with 'Inventory' component is required");
				}
			}
		}

		//TODO: Refactor this method to reuse code from OnItemManagerCollect() method
		protected virtual void OnItemManagerConsume(object sender, ItemEventArgs e) {
			
			ItemDataBaseList inventoryItemList = (ItemDataBaseList)Resources.Load("ItemDatabase");

			var item = inventoryItemList.getItemByName (e.Type);

			if (item == null) {
				Debug.LogErrorFormat ("The item {0} was not found on ItemDatabase", new string[]{ e.Type });
				return;
			}

			var playerInventory = GetComponent<PlayerInventory> ();

			if (playerInventory != null) {

				if (playerInventory.inventory != null) {

					var inventory = playerInventory.inventory.GetComponent<Inventory> ();

					if (inventory == null) {
						Debug.LogError ("The 'Inventory' component to addItemToInventory was not found");
						return;
					}

					inventory.deleteItemFromInventoryWithGameObject (item);
					inventory.stackableSettings ();

				} else {
					Debug.LogError ("The inventory gameObject with 'Inventory' component is required");
				}
			}
		}

		protected virtual void OnItemEquip(global::Item item) {

			if (item.itemType == ItemType.Weapon) {
				
				basicAttacks = GetComponentInChildren<BasicAttacks> ();
				
				if (basicAttacks != null && basicAttacks.attacks.Count > 0) {
					foreach (var attackData in basicAttacks.attacks) {
						
						foreach (var attribute in item.itemAttributes) {
							if (attribute.attributeName == "Damage") {
								
								attackData.damageAmount += attribute.attributeValue;
								attackData.animation = weaponAnimation.state;
								attackData.attackTime = weaponAnimation.time;
								if (weaponAnimation.hitBox != null) {
									attackData.hitBox = weaponAnimation.hitBox;
								}

								equipedItems.Add (item.itemName, attribute);
							}
						}
					}
						
					if (character == null) {
						Debug.LogWarning ("The Character component not found to init BasicAttack weapon HitBox");
					}
					basicAttacks.Init (character);
				}
			}
			
		}

		protected virtual void OnItemUnEquip(global::Item item) {
			
			if (item.itemType == ItemType.Weapon && equipedItems.Count > 0) {
				ItemAttribute attribute;

				if (equipedItems.TryGetValue (item.itemName, out attribute)) {
					
					foreach (var attackData in basicAttacks.attacks) {
						attackData.damageAmount -= attribute.attributeValue;
						attackData.animation = melleeAnimation.state;
						attackData.attackTime = melleeAnimation.time;

						if (melleeAnimation.hitBox != null) {
							attackData.hitBox = melleeAnimation.hitBox;
						}

						equipedItems.Remove (item.itemName);
					}
				}
			}
		}

		protected virtual bool OnItemUse(global::Item item) {

			bool useItem = true;

			if (item.itemType == ItemType.Consumable) {
				
				health = GetComponentInChildren<CharacterHealth> ();
				
				if (health != null) {
					
					foreach (var attribute in item.itemAttributes) {
						if (attribute.attributeName == "Health") {
							
							health.Heal (attribute.attributeValue);
						}

						//TODO: Avoid remove consumable item is a KEY
						if (attribute.attributeName == "Key" || attribute.attributeName == "Unlock") {

							if (itemManager != null) {
								
								if (item.maxStack > 1 || (!itemManager.HasItem (item.itemName) && item.maxStack == 1)) {
									if (ConsumeItem.tooltip != null) {
										
										ConsumeItem.tooltip.deactivateTooltip();
									}
									useItem = false;
									break;
								}
							}
						}
					}
				}
			}

			return useItem;
		}

		protected virtual void InitInventory(string itemName) {
			
		}

	}
		
	public class AttackAnimationData {

		public float time;
		public CharacterHitBox hitBox;
	}

	[System.Serializable]
	public class MelleeAnimationData : AttackAnimationData {
		
		public AnimationState state = AnimationState.ATTACK_PUNCH;
	}

	[System.Serializable]
	public class WeaponAnimationData : AttackAnimationData {

		public AnimationState state = AnimationState.ATTACK_STAB;
	}

}
