﻿using UnityEngine;
using System.Collections;
using System;
using PlatformerPro.Extras;
using PlatformerPro.Extensions;

namespace PlatformerPro
{

	[RequireComponent (typeof(BoxCollider2D), typeof(Rigidbody2D))]
	public class FrontDoor : EnterableDoorWithIcon
	{

		/// <summary>
		/// The player reference data
		/// </summary>
		[Header ("FrontDoor Settings")]
		[SerializeField]
		[Tooltip ("The player gameObject reference")]
		protected GameObject player;

		/// <summary>
		/// The fade animator reference to play
		/// when player enter this door
		/// </summary>
		[SerializeField]
		[Tooltip ("Optional animator controller to play a animation when player enter this door (e.g fade)")]
		protected Animator animator;

		[SerializeField]
		[Tooltip ("Name of the animation to play")]
		protected string animationState;

		[SerializeField]
		[Tooltip ("The default animation to play back when the player is entered the door")]
		protected string defaultAnimationState;

		[SerializeField]
		[Tooltip ("Run enterable actions after animation?")]
		protected bool enterAfterAnim = false;

		[SerializeField]
		[Tooltip ("How so far this door is checked to player enter?")]
		protected float limitDistance = 2.0f;

		[SerializeField]
		[Tooltip ("Change Player reverseHorizontal after enter on this door")]
		protected bool changeReverseHorizontal = false;

		#region unity hooks


		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		protected override void Awake ()
		{
			base.Awake ();

			GetComponent<BoxCollider2D> ().isTrigger = true;
			var rigidBody = GetComponent<Rigidbody2D> ();
			rigidBody.isKinematic = true;
			rigidBody.gravityScale = 0.0f;

			if (player == null) {
				Debug.LogError ("FrontDoor needs a player gameObject reference");
			}
		}


		/// <summary>
		/// Sent each frame where another object is within a trigger collider
		/// attached to this object (2D physics only).
		/// </summary>
		/// <param name="other">The other Collider2D involved in this collision.</param>
		void OnTriggerStay2D (Collider2D other)
		{

			if (other.CompareTag (player.tag)) {

				if (limitDistance > 0) {
					float currentDistance = Vector3.Distance (gameObject.transform.position, player.transform.position);

					if (currentDistance <= limitDistance) {

						ShowIcon ();

						CheckForEnter (player.GetComponent<Character> ());	
					}
				} else {
					ShowIcon ();
					CheckForEnter (player.GetComponent<Character> ());	
				}
			}
		}

		/// <summary>
		/// Reset entered state when the player leave the door
		/// </summary>
		/// <param name="other">Other.</param>
		void OnTriggerExit2D (Collider2D other)
		{

			if (iconInstance != null) {

				Destroy (iconInstance);
				iconInstance = null;
			}
		}

		#endregion

		/// <summary>
		/// Checks for enter.
		/// </summary>
		/// <param name="character">Character who may be entering.</param>
		protected override void CheckForEnter (Character character)
		{
			base.CheckForEnter (character);
		}

		/// <summary>
		/// Enter the door.
		/// </summary>
		/// <param name="character">Character.</param>
		protected override void DoEnter (Character character)
		{	
			if (!isEntered || alwaysEnter) {
				base.DoEnter (character);

				if (!locked) {
					
					isEntered = true;
				}
			}
		}

		/// <summary>
		/// Raises the door opened event.
		/// </summary>
		protected override void OnEntered (Character character)
		{
			if (animator != null) {

				if (enterAfterAnim) {
					StartCoroutine (PlayAnimation (() => {
						base.OnEntered (character);
					}));
				} else {
					base.OnEntered (character);
					StartCoroutine (PlayAnimation ());
				}
				
			} else {
				base.OnEntered (character);
			}

			if (changeReverseHorizontal) {
				if (character.Input is StandardInput) {
					var standardInput = (StandardInput)character.Input;
					((StandardInput)character.Input).reverseHorizontalAxis = !standardInput.reverseHorizontalAxis;
				}
			}
		}

		/// <summary>
		/// Called when one of the characters colliders collides with this platform. This should be overriden for platform specific behaviour.
		/// </summary>
		/// <param name="PlatformCollisionArgs">Arguments describing a platform collision.</param>
		/// <returns>true if character should be parented to this platform, otherwise false.</returns>
		protected override bool CustomCollide (PlatformCollisionArgs args)
		{
			return false;
		}

		/// <summary>
		/// A coroutine that play the default animation on entered front door,
		/// and execute a callback after ends.
		/// </summary>
		/// <returns>IEnumerator</returns>
		/// <param name="callback">Callback.</param>
		protected IEnumerator PlayAnimation (Action callback = null)
		{

			animator.Play (animationState);	

			AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo (0);
			yield return new WaitForSeconds (stateInfo.length);

			if (callback != null) {
				callback ();
			}

			if (defaultAnimationState.Length > 0) {
				animator.Play (defaultAnimationState);
			}
		}

		/// <summary>
		/// Go back to this origin zone
		/// </summary>
		public virtual void EnterTrigger ()
		{
			
			var cam = ((PlatformerPro.Extensions.SmoothCenteringTeleportCamera)PlatformCamera.DefaultCamera);

			if (cam.HasOriginZone) {
				cam.BackToOriginZone ();	
			}

		}
	}
}
