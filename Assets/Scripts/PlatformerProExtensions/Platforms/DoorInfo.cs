﻿using UnityEngine;
using System.Collections;

namespace PlatformerPro.Extensions
{	
	[System.Serializable]
	public class DoorInfo
	{
		public GameObject prefab;

		public GameObject parent;

		public Vector3 position = Vector3.zero;

		public Vector3 scale = Vector3.one;

		public Sprite focusSprite;

		public Sprite closedSprite;

		public float secondsToEnter;

	}
}

