﻿using UnityEngine;
using System.Collections;
using PlatformerPro;
using PlatformerPro.Extensions.Integrations.FMODSfx;

namespace PlatformerPro.Extensions {


	public class PlatformCustom : Platform {

		public GroundSfxType soundType;
		
		/// <summary>
		/// Occurs when the character collide with a platform.
		/// </summary>
		public event System.EventHandler <PlatformCollisionEventArgs> PlatformCollided;

		/// <summary>
		/// Called when one of the characters colliders collides with this platform.
		/// Overrided to call "PlatformCollided" event
		/// </summary>
		/// <param name="PlatformCollisionArgs">Arguments describing a platform collision.</param>
		/// <returns>true if character should be parented to this platform, otherwise false.</returns>
		protected override bool CustomCollide(PlatformCollisionArgs args)
		{
			if (PlatformCollided != null) {
				
				var eventArgs = new PlatformCollisionEventArgs ();
				eventArgs.Character = args.Character;
				eventArgs.Penetration = args.Penetration;
				eventArgs.RaycastCollider = args.RaycastCollider;

				PlatformCollided (this, eventArgs);
			}

			return base.CustomCollide(args);
		}

	}
}
