﻿using UnityEngine;
using System.Collections;
using PlatformerPro;
using PlatformerPro.Extras;

namespace PlatformerPro.Extensions {
	
	public class DoorActions : MonoBehaviour {

		/// <summary>
		/// The Targets that receive the triggers actions.
		/// </summary>
		[HideInInspector]
		public TriggerTarget[] receivers;

		/// <summary>
		/// Init this door.
		/// </summary>
		public virtual void Init()
		{
			if (receivers != null && receivers.Length > 0)
			{
				foreach(TriggerTarget t in receivers) 
				{
					t.platform = t.receiver.GetComponent<Platform>();
					t.cameraZone = t.receiver.GetComponent<CameraZone>();
					t.combiner = t.receiver.GetComponent<TriggerCombiner>();
				}
			}
		}

		// TODO: NEED'S REFACTORY TO REUSE THIS ACTIONS FROM "TRIGGER" CLASS AND AVOID DRY
		/// <summary>
		/// Perform the same actions of <see cref="PlatformerPro.Trigger"/> class
		/// </summary>
		/// <param name="sender">Sender delegate event.</param>
		/// <param name="doorArgs">Door arguments.</param> 
		public virtual void EnterAction(object sender, DoorEventArgs doorArgs)
		{
			if (enabled)
			{
				for (int i = 0; i < receivers.Length; i++)
				{
					if (receivers[i] != null)
					{
						switch(receivers[i].enterAction)
						{
						case TriggerActionType.SEND_MESSAGE:
							receivers[i].receiver.SendMessage("EnterTrigger", SendMessageOptions.DontRequireReceiver);
							break;
						case TriggerActionType.ACTIVATE_PLATFORM:
							receivers[i].platform.Activate(doorArgs.Character);
							break;
						case TriggerActionType.DEACTIVATE_PLATFORM:
							receivers[i].platform.Deactivate(doorArgs.Character);
							break;
						case TriggerActionType.ACTIVATE_GAMEOBJECT:
							receivers[i].receiver.SetActive(true);
							break;
						case TriggerActionType.DEACTIVATE_GAMEOBJECT:
							receivers[i].receiver.SetActive(false);
							break;
						case TriggerActionType.CHANGE_CAMERA_ZONE:
							PlatformCamera.DefaultCamera.ChangeZone(receivers[i].cameraZone);
							break;
						case TriggerActionType.SWITCH_SPRITE:
							// TODO This should not be done here
							SpriteRenderer spriteRenderer = receivers[i].receiver.GetComponentInChildren<SpriteRenderer>();
							if (spriteRenderer != null)
							{
								spriteRenderer.sprite = receivers[i].newSprite;
							}
							else
							{
								Debug.LogError ("Trigger tried to switch sprite but no SpriteRenderer was found");
							}
							break;
						case TriggerActionType.SHOW_DIALOG:
							UIDialog dialog = receivers[i].receiver.GetComponentInChildren<UIDialog>();
							if (dialog != null)
							{
								dialog.ShowDialog(transform);
							}
							else
							{
								Debug.LogError ("Trigger tried to show dialog but no UIDialog was found.");
							}
							break;
						case TriggerActionType.HIDE_DIALOG:
							UIDialog dialogToHide = receivers[i].receiver.GetComponentInChildren<UIDialog>();
							if (dialogToHide != null)
							{
								dialogToHide.HideDialog();
							}
							else
							{
								Debug.LogError ("Trigger tried to show dialog but no UIDialog was found.");
							}
							break;
						case TriggerActionType.OPEN_DOOR:
						case TriggerActionType.FORWARD:
							Door doorToOpen = receivers[i].receiver.GetComponent<Door>();
							if (doorToOpen != null)
							{
								doorToOpen.Open(doorArgs.Character);
							}
							else
							{
								Debug.LogError ("Trigger tried to open door but no Door was found.");
							}
							break;
						case TriggerActionType.CLOSE_DOOR:
							Door doorToClose = receivers[i].receiver.GetComponent<Door>();
							if (doorToClose != null)
							{
								doorToClose.Close(doorArgs.Character);
							}
							else
							{
								Debug.LogError ("Trigger tried to close door but no Door was found.");
							}
							break;
						case TriggerActionType.ACTIVATE_SPAWN_POINT:
							RespawnPoint point = receivers[i].receiver.GetComponentInChildren<RespawnPoint>();
							if (point != null) point.SetActive();
							else
							{
								Debug.LogError ("Trigger tried to activate respawn point but no RespawnPoint was found");
							}
							break;
						}
					}
				}

			}

		}
	}
}
