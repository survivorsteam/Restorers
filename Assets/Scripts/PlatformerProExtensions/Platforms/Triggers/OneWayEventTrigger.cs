﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace PlatformerPro {
	
	public class OneWayEventTrigger : OneWayTrigger {

		[System.Serializable]
		public class EnteredEvent : UnityEvent<CharacterEventArgs> {}

		[SerializeField]
		protected EnteredEvent onEnteredEvent = new EnteredEvent ();

		public virtual void OnEnable() {

			TriggerEntered += HandleCustomEvent;
		}

		public virtual void OnDisable() {
			
			TriggerEntered -= HandleCustomEvent;
		}

		protected virtual void HandleCustomEvent(object sender, CharacterEventArgs e) {
			onEnteredEvent.Invoke (e);
		}
	}
}
