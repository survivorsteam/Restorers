﻿using UnityEngine;
using System.Collections;
using PlatformerPro;

namespace PlatformerPro.Extras {
	
	public class ObjectTrigger : Trigger {

		[SerializeField]
		protected GameObject requiredObject;

		/// <summary>
		/// Unity enable hook
		/// </summary>

		void OnEnable()
		{
			if (!Application.isPlaying)
			{
				if (GetComponent<Collider2D>() == null)
				{
					BoxCollider2D boxCollider = gameObject.AddComponent<BoxCollider2D>();
					boxCollider.isTrigger = true;
				}
			}
		}

		/// <summary>
		/// Unity start hook.
		/// </summary>
		void Start () {
			Init();
		}

		/// <summary>
		/// Unity 2D trigger hook
		/// </summary>
		/// <param name="other">Other.</param>
		void OnTriggerEnter2D(Collider2D other)
		{
			if (other.CompareTag (requiredObject.tag)) {
				Character character = null;
				var pushable = other.GetComponentInParent<PushablePullable25D> ();
				if (pushable != null) {
					character = pushable.character;
				}

				EnterTrigger(character);
			}
		}

		/// <summary>
		/// Unity 2D trigger hook
		/// </summary>
		/// <param name="other">Other.</param>
		void OnTriggerExit2D(Collider2D other)
		{
			
			if (other.CompareTag (requiredObject.tag)) {
				Character character = null;
				var pushable = other.GetComponent<PushablePullable25D> ();
				if (pushable != null) {
					character = pushable.character;
				}

				LeaveTrigger(character);
			}
		}
	}
}
