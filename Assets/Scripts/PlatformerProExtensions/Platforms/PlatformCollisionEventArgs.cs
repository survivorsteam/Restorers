﻿using UnityEngine;
using System;

namespace PlatformerPro.Extensions
{
	public class PlatformCollisionEventArgs : System.EventArgs
	{
		/// <summary>
		/// The character that collided.
		/// </summary>
		public IMob Character
		{
			get; set;
		}

		/// <summary>
		/// The collider that collided.
		/// </summary>
		public BasicRaycast RaycastCollider
		{
			get; set;
		}

		/// <summary>
		/// The depth of the penetration.
		/// </summary>
		public float Penetration
		{
			get; set;
		}

	}
}

