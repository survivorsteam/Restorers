﻿using UnityEngine;
using System.Collections;
using PlatformerPro;

namespace PlatformerPro.Extensions {

	[RequireComponent(typeof(EdgeCollider2D))]
	public class InclinedWalk : Platform {

		/// <summary>
		/// The player reference data
		/// </summary>
		[Header ("InclinedPlatform Settings")]
		[SerializeField]
		[Tooltip ("The player gameObject reference")]
		protected GameObject player;

		public DoorInfo info;

		protected GameObject infoInstance;

		[SerializeField]
		protected bool triggerColliderOnInit = false;

		void Awake() 
		{	
			var collider = GetComponent<EdgeCollider2D> ();

			if (triggerColliderOnInit) {
				collider.isTrigger = true;
			}

		}

		void OnTriggerStay2D (Collider2D other)
		{
			if (other.CompareTag (player.tag)) {
				ShowIcon ();
			}
		}

		/// <summary>
		/// Reset entered state when the player leave the door
		/// </summary>
		/// <param name="other">Other.</param>
		void OnTriggerExit2D (Collider2D other)
		{

			if (infoInstance != null) {
				
				Destroy (infoInstance);
				infoInstance = null;
			}
		}

		/// <summary>
		/// Shows the icon if defined on inspector.
		/// </summary>
		virtual public void ShowIcon()
		{
			if (infoInstance != null) {
				infoInstance.SetActive (true);
			} else {

				if (info.prefab != null && GetComponentInChildren<SpriteRenderer> () == null) {

					infoInstance = Instantiate (info.prefab);

					infoInstance.transform.SetParent (info.parent != null ? info.parent.transform : transform);
					if (info.position != Vector3.zero) {

						infoInstance.transform.localPosition = info.position;
					}

					if (info.scale != Vector3.one) {

						infoInstance.transform.localScale = info.scale;
					}

				}
			}

		}

	}
}
