﻿using UnityEngine;
using System.Collections;
using PlatformerPro;

/// <summary>
/// An object that can be pushed and pulled.
/// 
/// This is a extension of Pushable.cs PlatformerPro core
/// script, to move 3D objects with a 2D character colliders
/// </summary>
public class PushablePullable25D : PushablePullable {

	/// <summary>
	/// A 3D gameObject to move together with this gameObject.
	/// </summary>
	[SerializeField]
	[Tooltip("Another object to push/pull too. Recommendend to 2.5D games with 3D objects that needs use 2D Pyhisics ")]
	protected GameObject relatedObject;

	/// <summary>
	/// The cached character component that pushing a box
	/// </summary>
	[HideInInspector]
	public Character character;

	/// <summary>
	/// Run before Start()
	/// Add a RigidBody to the 3D related object
	/// </summary>
	void Awake() {
		if (relatedObject != null) {
			relatedObject.AddComponent<Rigidbody> ().isKinematic = true;
		}
	}

	/// <summary>
	/// Unity Fixed Update, does most of the work.
	/// 
	/// Overrides to move the related 3D object too
	/// </summary>
	void FixedUpdate () {
		
		physicsUpdatedThisFrame = true;
		if (targetSet) {
			rigidbody2D.mass = 1;
			// Simulate a simple gravity like force so that we fall even though we are using MovePosition()
			rigidbody2D.velocity += new Vector2(0, Physics2D.gravity.y * Time.fixedDeltaTime);
			targetPosition += new Vector2(0, rigidbody2D.velocity.y * Time.fixedDeltaTime);
			expectedY = targetPosition.y + rigidbody2D.position.y;

			// Move to position
			Vector2 movePos = (Vector2)rigidbody2D.position + targetPosition;
			rigidbody2D.MovePosition (movePos);

			if (relatedObject != null) {
				
				Vector3 newRelatedPos = new Vector3(movePos.x, relatedObject.transform.position.y, relatedObject.transform.position.z);
				relatedObject.GetComponent<Rigidbody>().MovePosition(newRelatedPos);
			}

			targetSet = false;
		}
		else
		{
			rigidbody2D.mass = mass;
		}
		if (forceSet)
		{
			rigidbody2D.AddForce (force, ForceMode2D.Force);
			if (relatedObject != null) {
				relatedObject.GetComponent<Rigidbody>().AddForce(force, ForceMode.Force);
			}
			forceSet = false;
		}

		// Apply additional dynamic friction
		if (additionalDynamicFriction > 0.0f && !rigidbody2D.IsSleeping() && Mathf.Abs (rigidbody2D.velocity.x) > 0.1f)
		{
			rigidbody2D.AddForce(new Vector2(-rigidbody2D.velocity.x * additionalDynamicFriction * Time.fixedDeltaTime, 0), ForceMode2D.Impulse);
			if (relatedObject != null) {
				Vector3 frictionForce = new Vector3 (-rigidbody2D.velocity.x * additionalDynamicFriction * Time.fixedDeltaTime, 0);
				relatedObject.GetComponent<Rigidbody>().AddForce(frictionForce, ForceMode.Impulse);
			}
		}
	}

	/// <summary>
	/// Push this pushable.
	/// </summary>
	/// <param name="character">Character who is pushing..</param>
	/// <param name="amount">Amount to push.</param>
	public override void Push(IMob character, Vector2 amount, bool pushAsForce){
		
		base.Push (character, amount, pushAsForce);

		if (character != null && character is Character) {
			
			this.character = character as Character;
		}
	}
}
