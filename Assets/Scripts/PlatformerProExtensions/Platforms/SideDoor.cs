﻿using UnityEngine;
using System.Collections;

namespace PlatformerPro
{
	public class SideDoor : EnterableDoorWithIcon {

		protected BasicRaycast rayCastCollider;

		void Update() {
			
			if (rayCastCollider != null && iconInstance != null) {
				RaycastHit2D hit = rayCastCollider.GetRaycastHit ();
				if (hit.collider == null) {
					float currentDistance = Vector3.Distance (transform.position, FindObjectOfType<Character> ().transform.position);
					if (currentDistance > 7) {
						HideIcon ();
					} else {
						iconInstance.SetActive (false);
					}

				}
			}
		}
		
		/// <summary>
		/// Called when one of the characters colliders collides with this platform. This should be overriden for platform specific behaviour.
		/// </summary>
		/// <param name="PlatformCollisionArgs">Arguments describing a platform collision.</param>
		/// <returns>true if character should be parented to this platform, otherwise false.</returns>
		protected override bool CustomCollide(PlatformCollisionArgs args)
		{
			if ((args.RaycastCollider.RaycastType & RaycastType.SIDES) == args.RaycastCollider.RaycastType && args.Character is Character) {
				rayCastCollider = args.RaycastCollider;
				ShowIcon ();
				CheckForEnter ((Character)args.Character);
			}
			return false;
		}
	}
}
