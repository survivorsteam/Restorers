﻿using UnityEngine;
using System.Collections;
using PlatformerPro.Extensions;

namespace PlatformerPro {

	[RequireComponent(typeof(DoorActions))]
	public class EnterableDoorWithIcon : EnterableDoor {

		public DoorInfo icon;

		public bool locked = false;

		public bool alwaysEnter = false;

		/// <summary>
		/// The Targets that receive the triggers actions.
		/// </summary>
		public TriggerTarget[] receivers;

		/// <summary>
		/// The player is entered this door?
		/// </summary>
		[HideInInspector]
		public bool isEntered = false;


		protected GameObject iconInstance;

		protected DoorActions actions;

		#region unity hooks

		virtual protected void Awake() {
			
			// TODO AVOID USE THIS CLASS LIKE A COMPONENT. USE LIKE A SIMPLE CLASS
			actions = GetComponent<DoorActions> ();
		}

		protected virtual void OnEnable()
		{
			Entered += actions.EnterAction;
		}

		protected virtual void OnDisable()
		{
			Entered -= actions.EnterAction;
			HideIcon ();
		}

		#endregion

		/// <summary>
		/// Init this door.
		/// </summary>
		override protected void Init()
		{
			actions.receivers = receivers;
			actions.Init ();

			base.Init();
		}

		/// <summary>
		/// Enter the door.
		/// </summary>
		/// <param name="character">Character.</param>
		protected override void DoEnter(Character character)
		{
			if (iconInstance != null && icon.focusSprite != null) {
				iconInstance.GetComponent<SpriteRenderer> ().sprite = icon.focusSprite;
				StartCoroutine (EnterInSeconds (icon.secondsToEnter, character));
			} else {

				base.DoEnter (character);
			}
		}

		/// <summary>
		/// Enters into this door in seconds.
		/// </summary>
		/// <returns>The in seconds.</returns>
		/// <param name="seconds">Seconds.</param>
		/// <param name="character">Character.</param>
		IEnumerator EnterInSeconds(float seconds, Character character) 
		{
			yield return new WaitForSeconds (seconds);

			if (!locked) {
				
				base.DoEnter (character);
			} else {
				HideIcon ();
			}
		}

		/// <summary>
		/// Raises the door opened event.
		/// </summary>
		protected override void OnEntered(Character character)
		{
			base.OnEntered (character);

			HideIcon (() => isEntered = true);

		}

		/// <summary>
		/// Open the door.
		/// </summary>
		public override void Open(Character character) 
		{
			// Check additional conditions
			if (conditions != null)
			{
				foreach (AdditionalCondition condition in conditions)
				{
					if (!condition.CheckCondition (character, this))
						return;
				}
			}

			if (keyType == null || keyType == "")
			{
				DoOpen (character);
			} 
			else
			{
				ItemManager itemManager = character.GetComponentInChildren<ItemManager> ();
				if (itemManager != null)
				{
					//TODO Remove item from InventoryMaster
					//TODO Review the Open/Enter/Closed flow and when the icons is showed
					if (itemManager.ItemCount (keyType) > 0) {
						DoOpen (character);
						itemManager.ConsumeItem (keyType, 1);
					} else {
						ShowClosedIcon ();
					}
				}
				else
				{
					Debug.LogError("Door requires a key but there is no item manager in the scene.");
				}
			}
		}

		/// <summary>
		/// Shows the icon if defined on inspector.
		/// </summary>
		public virtual void ShowIcon()
		{
			if (iconInstance != null) {
				iconInstance.SetActive (true);
			} else {

				if (icon.prefab != null && GetComponentInChildren<SpriteRenderer> () == null) {
					
					iconInstance = Instantiate (icon.prefab);
					
					iconInstance.transform.SetParent (icon.parent != null ? icon.parent.transform : transform);
					if (icon.position != Vector3.zero) {
						
						iconInstance.transform.localPosition = icon.position;
					}

					if (icon.scale != Vector3.one) {
						
						iconInstance.transform.localScale = icon.scale;
					}
					
				}
			}

		}

		public virtual void ShowClosedIcon() {
			if (iconInstance == null) {
				ShowIcon ();
			} else if(icon.closedSprite != null) {
				iconInstance.GetComponent<SpriteRenderer> ().sprite = icon.closedSprite;
			}
		}

		public virtual void HideIcon(System.Action cb = null)
		{
			if (iconInstance != null) {
				Destroy (iconInstance);
				iconInstance = null;

				if (cb != null) {
					
					cb ();
				}
			}
		}
	}
}
