﻿using UnityEngine;
using System.Collections;
using PlatformerPro;

namespace PlatformerPro.Extensions {

	/// <summary>
	/// Custom Stairs class
	/// 
	/// Changed to Gizmos draw follow the Z axis 
	/// of the gameObject attached with this component
	/// </summary>
	public class StairsCustom : Stairs {

		/// <summary>
		/// Draw handles for showing extents/stairs. 
		/// </summary>
		void OnDrawGizmos() {
			Init ();
			if (stepSize.x > 0 && stepSize.y > 0)
			{
				Gizmos.color = Color.green;
				Vector3 currentPos = new Vector3(leftPoint.x, leftPoint.y, transform.position.z);
				if (stepDirection > 0) 
				{
					for (int i = 0; i < stepCount; i++)
					{
						Gizmos.DrawLine (currentPos, currentPos + new Vector3 (0, stepSize.y * stepDirection));
						currentPos += new Vector3 (0, stepSize.y * stepDirection);
						Gizmos.DrawLine (currentPos, currentPos + new Vector3 (stepSize.x, 0));
						currentPos += new Vector3 (stepSize.x, 0);
					}
				}
				else 
				{
					for (int i = 0; i < stepCount; i++)
					{
						Gizmos.DrawLine (currentPos, currentPos + new Vector3 (stepSize.x, 0));
						currentPos += new Vector3 (stepSize.x, 0);
						Gizmos.DrawLine (currentPos, currentPos + new Vector3 (0, stepSize.y * stepDirection));
						currentPos += new Vector3 (0, stepSize.y * stepDirection);
					}
				}
			}
		}
	}
}
