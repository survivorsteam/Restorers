﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneLoader : MonoBehaviour {

	/// <summary>
	/// The name of the scene.
	/// </summary>
	[SerializeField]
	protected string sceneName;

	/// <summary>
	/// The loading screen panel to show while the scene is loading.
	/// </summary>
	[SerializeField]
	protected GameObject loadingScreen;

	/// <summary>
	/// The seconds to wait before starts load scene.
	/// </summary>
	[SerializeField]
	protected float waitSeconds;

	/// <summary>
	/// Stop audio source from this or parent gameObject?
	/// </summary>
	[SerializeField]
	protected bool stopAudioSource;

	/// <summary>
	/// The audio source component cache reference
	/// </summary>
	protected AudioSource audioSource;

	public virtual void Awake() {

		audioSource = GetComponentInParent<AudioSource> ();
	
	}

	/// <summary>
	/// Loads the scene async and hide the loading panel after that.
	/// </summary>
	/// <returns>The IEnumerator to StartCoroutine().</returns>
	/// <param name="name">The name of the scene to load.</param>
	public IEnumerator LoadAsyncScene(string name) {
		var asyncOp = SceneManager.LoadSceneAsync (name);

		if (asyncOp.isDone) {
			
			if (loadingScreen != null && loadingScreen.activeSelf) {
				loadingScreen.SetActive (false);
			}
		}

		yield return asyncOp;

	}

	/// <summary>
	/// Waits for secods an run a callback after that
	/// </summary>
	/// <returns>The time.</returns>
	/// <param name="seconds">Seconds.</param>
	/// <param name="callback">Callback.</param>
	public virtual IEnumerator WaitTime(float seconds, System.Action callback = null) {
		
		yield return new WaitForSeconds(seconds);
		
		if (callback != null) {
			callback ();
		}
		
	}

	/// <summary>
	/// Shows the scene.
	/// </summary>
	/// <param name="name">Name.</param>
	public virtual void ShowScene(string name = null) {
		
		name = name != null ? name : this.sceneName;

		if (name == null) {
			Debug.LogError ("A Scene name is required to load async");
			return;
		}

		if (loadingScreen != null) {
			if (audioSource != null && stopAudioSource) {
				audioSource.Stop ();
			}
			loadingScreen.SetActive (true);
		}

		if (waitSeconds != null) {
			
			StartCoroutine (WaitTime (waitSeconds, () => {
				StartCoroutine (LoadAsyncScene (name));
			}));
		} else {
			StartCoroutine (LoadAsyncScene (name));
		}
			
	}

}
