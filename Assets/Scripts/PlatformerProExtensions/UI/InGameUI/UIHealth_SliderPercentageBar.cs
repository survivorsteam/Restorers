﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using PlatformerPro.Extras;
using PlatformerPro;
using AnimationState = PlatformerPro.AnimationState;

namespace PlatformerPro.Extensions {
	
	public class UIHealth_SliderPercentageBar : UIHealth_PercentageBar {

		/// <summary>
		/// Reference to the parent with Unity UI.Slider component;
		/// </summary>
		protected Slider sliderFill;

		/// <summary>
		/// Updates this bar only when the character is damaged.
		/// </summary>
		protected bool updateOnlyDamaged = false;

		/// <summary>
		/// Updates this bar on each frame.
		/// </summary>
		protected bool updateEveryTime = false;

		void Update()
		{
			if (updateEveryTime && characterHealth != null)
				UpdateImage ();
		}

		/// <summary>
		/// Overrides the parent method to update image on "Damaged" and "Healed" events
		/// </summary>
		override protected void Init() 
		{
			sliderFill = GetComponentInParent<Slider> ();
			base.Init ();

			if (characterHealth != null) {
				characterHealth.Damaged += HandleDamaged;

				if (!updateOnlyDamaged) {
					characterHealth.Healed += HandleHealed;	
				}

			}
		}
		/// <summary>
		/// Overrides the parent method to use a slider to fill image.
		/// </summary>
		override protected void UpdateImage()
		{
			float maxPercentage = 100.0f;

			if (barImage.type == Image.Type.Filled) {
				if (sliderFill != null) {
					sliderFill.value = maxPercentage * characterHealth.CurrentHealthAsPercentage;
				} else {
					barImage.fillAmount = maxPercentage * characterHealth.CurrentHealthAsPercentage;
				}

			} else {

				base.UpdateImage ();
			}
		}

		/// <summary>
		/// Handles the character being damaged.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		virtual protected void HandleDamaged(object sender, DamageInfoEventArgs e) 
		{
			UpdateImage ();

		}

		/// <summary>
		/// Handles the character being healed.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		virtual protected void HandleHealed(object sender, PlatformerPro.HealedEventArgs e)
		{
			UpdateImage ();
		}

	}
}
