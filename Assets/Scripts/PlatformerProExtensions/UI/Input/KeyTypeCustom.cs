﻿using System;

namespace PlatformerPro.Extensions
{
	/// <summary>
	/// Custom key types and events to player listen.
	/// </summary>
	public enum KeyTypeCustom
	{
		NONE,
		UP,
		DOWN,
		VERTICAL_AXIS,
		LEFT,
		RIGHT,
		HORIZONTAL_AXIS,
		JUMP,
		RUN,
		ACTION,
		PAUSE,
		ALT_VERTICAL_AXIS,
		ALT_HORIZONTAL_AXIS,
		OPEN_DOOR,
		PUSH,
		PULL,
		PUSH_OR_PULL = PUSH ^ PULL
	}
}

