﻿using UnityEngine;
using System.Collections;
using PlatformerPro;
using AnimationState = PlatformerPro.AnimationState;

namespace PlatformerPro.Extensions {

	/// <summary>
	/// UI overlay like a dialog to display that listen for a event or a keypressed.
	/// Great to show tutorials, icons and alerts to the player
	/// </summary>
	public class UITipOverlay : UIInputOverlay {

		/// <summary>
		/// The player gameObject with Character component to watch input keys
		/// </summary>
		[SerializeField]
		[Tooltip("The player gameObject with Character component to watch input keys")]
		public GameObject player;

		/// <summary>
		/// The type of the key to check if the player is pressed
		/// </summary>
		[SerializeField]
		[Tooltip("The type of the key to check if the player is pressed")]
		protected KeyTypeCustom keyType;

		/// <summary>
		/// Actions to perform when hide this tip
		/// </summary>
		[SerializeField]
		[Tooltip("Actions to perform when hide this tip")]
		protected UITipAction action;

		/// <summary>
		/// The index of the action button to check. Only if Action = ACTION
		/// </summary>
		[SerializeField]
		[Tooltip("The index of the action button to check. Only if Action = ACTION")]
		protected int actionButton = 0;

		/// <summary>
		/// Show this tip only if a input events is released (key press, click...
		/// </summary>
		[SerializeField]
		[Tooltip("Show this tip only if a input events is released (key press, click...")]
		protected bool watchInput = true;

		/// <summary>
		/// Show this tip on start the game?
		/// </summary>
		[SerializeField]
		[Tooltip("Show this tip on start the game?")]
		protected bool showOnStart = false;

		//TODO: Change the visibility to protected and create a property to access
		/// <summary>
		/// The door to verify if a player enter. Only if KeyType = OPEN_DOOR
		/// </summary>
		[HideInInspector]
		public EnterableDoor door;

		/// <summary>
		/// Cached character reference
		/// </summary>
		protected Character character;

		/// <summary>
		/// The cached instance gameObject.name of the last
		/// cloned obj on "visibleContent".
		/// Find this in hierarchy
		/// and remove them if exists
		/// </summary>
		protected string instanceName = "";

		protected bool addedEvent = false;

		/// <summary>
		/// Gets or sets the name of the gameObject
		/// instance of this prefab
		/// </summary>
		/// <value>The name of the instance.</value>
		public string InstanceName {

			get {
				return instanceName;
			}

			set {
				if (!value.Contains ("Clone")) {
					Debug.LogError ("The instanceName needs be a instance of prefab");
				} else {
					instanceName = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the action.
		/// </summary>
		/// <value>The action.</value>
		public UITipAction Action {

			get {
				return action;
			}

			set {
				action = value;
			}
		}

		#region unity hooks

			void Awake () {

				if (player != null) {
					character = player.GetComponent<Character> ();

					if (character == null) {
						Debug.LogError ("The player gameObject needs a Character component");
					}
				}
				
				if (showOnStart) {
					DoShow ();
				}
			}
		
		// Update is called once per frame
		void Update () {

			if (watchInput) {

				if (character.Input.AnyKey) {
					CheckForKey ();
				} else {
					CheckForEvent ();

				}
			}
				
		}

		#endregion

		/// <summary>
		/// Verifies the key pressed and hide this tip
		/// </summary>
		virtual public void CheckForKey() {
			
			switch (keyType) {

			case KeyTypeCustom.HORIZONTAL_AXIS:

				if (character.Input.HorizontalAxisDigital != 0 
					&& (character.Input.RunButton != ButtonState.HELD && character.Input.RunButton != ButtonState.DOWN)) {
					DoHide ();
				}
				break;

			case KeyTypeCustom.VERTICAL_AXIS:

				if (character.Input.VerticalAxisDigital != 0) {
					DoHide ();
				}
				break;

			case KeyTypeCustom.ACTION:
				if (character.Input.GetActionButtonState (actionButton) == ButtonState.DOWN) {
					DoHide ();
				}
				break;

			case KeyTypeCustom.JUMP:

				if (character.Input.JumpButton == ButtonState.DOWN) {
					DoHide ();
				}
				break;

			case KeyTypeCustom.RUN:

				if (character.Input.RunButton == ButtonState.HELD) {
					DoHide ();
				}
				break;
			default:
				break;
			}
		}

		virtual public void CheckForEvent() {
			
			switch (keyType) {

				case KeyTypeCustom.OPEN_DOOR:
					if (door != null) {

						if (!addedEvent) {

							door.Entered += (object sender, DoorEventArgs e) => {
								if (e.DoorState == DoorState.OPEN) {

									DoHide ();
								}
							};
							addedEvent = true;

						}
					} else {
						Debug.LogErrorFormat ("The '{0}' of '{1}' is required for action OPEN_DOOR", new[]{"door", this.name});
					}

					break;

				case KeyTypeCustom.PUSH:
				case KeyTypeCustom.PULL:
					if (!addedEvent) {
						character.ChangeAnimationState += (object sender, AnimationEventArgs e) => {
							if ((e.State == AnimationState.PUSH || e.State == AnimationState.PULL)) {
								DoHide ();
							}
						};
						addedEvent = true;
							
					}
					break;

				default:
					break;
			}
		}

		// TODO Perform the action using Events (delegates)
		/// <summary>
		/// Hide this tip with a specific action.
		/// </summary>
		virtual public void DoHide(){

			switch (action) {

				case UITipAction.ACTIVATE_GAMEOBJECT:
					visibleContent.SetActive (true);
					break;
				
				case UITipAction.DEACTIVATE_GAMEOBJECT:
					visibleContent.SetActive (false);
					break;

				case UITipAction.DESTROY:
				
					if (instanceName.Length > 0) {

						//Destroy only cloned instances instead of the prefab
						Transform child = visibleContent.transform.Find(instanceName);

						if (child != null) {
							
							visibleContent.SetActive (false);
							Destroy (child.gameObject);
						}

					} else if (visibleContent.activeInHierarchy && visibleContent.GetComponentInChildren<UITipOverlay> () != null){
						
						visibleContent.SetActive (false);
						Destroy (gameObject);
					}
					break;

				default:
					break;
			}

		}

		/// <summary>
		/// Shows this tip on parent defined by <see cref="UITipOverlay.visibleContent"/>
		/// </summary>
		virtual public void DoShow(){
			
			var tip = visibleContent.GetComponentInChildren<UITipOverlay> ();

			if (tip == null) {
				
				gameObject.transform.SetParent (visibleContent.transform);
				gameObject.transform.localScale = Vector3.one;
				gameObject.transform.localPosition = Vector3.zero;
				gameObject.SetActive (true);
				visibleContent.SetActive (true);

			}

		}
	}
}
