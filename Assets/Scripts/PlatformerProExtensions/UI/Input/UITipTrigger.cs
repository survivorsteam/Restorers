﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using PlatformerPro;

namespace PlatformerPro.Extensions {

	/// <summary>
	/// Trigger to show a UITipOverlay
	/// </summary>
	[RequireComponent(typeof(BoxCollider2D), typeof(Rigidbody2D))]
	public class UITipTrigger : MonoBehaviour {

		/// <summary>
		/// A parent container to show the tooltip on their visibleContent. Needs a object created on hierarchy
		/// </summary>
		[SerializeField]
		[Tooltip("A parent container to show the tooltip on their visibleContent. Needs a object created on hierarchy")]
		protected GameObject tipContainer;

		/// <summary>
		/// A UI tooltip game object prefab to show
		/// </summary>
		[SerializeField]
		[Tooltip("A UI tooltip game object prefab to show")]
		protected GameObject tipPrefab;

		/// <summary>
		/// A Input control action to listen to hide/remove the UITip
		/// </summary>
		[SerializeField]
		[Tooltip("A Input control action to listen to hide/remove the UITip")]
		protected UITipAction actionOnInput;

		/// <summary>
		/// The Player reference to check triggers
		/// </summary>
		[SerializeField]
		[Tooltip("The Player reference to check triggers")]
		protected GameObject player;

		/// <summary>
		/// Hide the tip on exit this trigger?
		/// </summary>
		[SerializeField]
		[Tooltip("Hide the tip on exit this trigger?")]
		protected bool hideOnExit;

		/// <summary>
		/// Show the tip only once?
		/// </summary>
		[SerializeField]
		[Tooltip("Show the tip only once?")]
		protected bool showOnce;

		/// <summary>
		/// The door to verify if a player enter. Only if tipPrefab.KeyType == OPEN_DOOR
		/// </summary>
		[SerializeField]
		[Tooltip("The door to verify if a player enter. Only if tipPrefab.KeyType == OPEN_DOOR")]
		protected EnterableDoor door;

		/// <summary>
		/// Controls if the tip is showed on the scene hierarchy
		/// </summary>
		protected bool isShowed = false;
	
		/// <summary>
		/// Cached tip gameObject
		/// </summary>
		protected GameObject tipOverlayObj;

		/// <summary>
		/// Cached UITipOverlay component of a gameObject
		/// instance "clone" created from tipPrefab
		/// </summary>
		protected UITipOverlay tipComponent;

		#region unity hooks

		void Awake() {
			
			if (tipPrefab == null) {
				Debug.LogError ("The prefab to show a UITip is required");	
			} else {
				

				if (tipPrefab.GetComponentInChildren<UITipOverlay>() == null) {
					Debug.LogError ("The tip prefab '"+ tipPrefab.name +"' needs a UITipOverlay script component");
				}
			}

			if (tipContainer != null) {
				#if UNITY_EDITOR
				if (PrefabUtility.GetPrefabType (tipContainer) == PrefabType.Prefab) {
					string[] args = new[]{"tipContainer", this.name};
					Debug.LogErrorFormat ("The '{0}' of '{1}' cannot be a prefab!", args);
				}
				#endif
			}

		}
			

		void OnTriggerStay2D(Collider2D other) {


			if (other.CompareTag (player.tag)) {

				if (showOnce && isShowed) {
					return;
				}
					
				tipOverlayObj = Instantiate (tipPrefab);
				tipComponent = tipOverlayObj.GetComponent<UITipOverlay> ();

				if (door != null) {
					tipComponent.door = door;
				}
				tipComponent.Action = actionOnInput;
				tipComponent.InstanceName = tipOverlayObj.name;

				//Override prefabs of scene hierarchy gameObjects
				if (tipContainer != null) {
					
					tipComponent.visibleContent = tipContainer;
					tipComponent.player = player;
				}

				tipComponent.DoShow ();

				isShowed = true;
			}
		}

		void OnTriggerExit2D(Collider2D other) {
			
			if (hideOnExit) {
				tipComponent.DoHide ();
			}
		}

		#endregion
	}
}
