﻿using System;

namespace PlatformerPro.Extensions
{
	public enum UITipAction
	{
		ACTIVATE_GAMEOBJECT,
		DEACTIVATE_GAMEOBJECT,
		DESTROY
	}
}

