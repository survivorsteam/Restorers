﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace PlatformerPro.Extensions {
	
	public class UIShowText : MonoBehaviour {

		[SerializeField]
		protected Text[] textFields;

		[SerializeField]
		protected Button buttonChild;

		virtual public void ShowText(int index) {
			
			for (int i = 0; i < textFields.Length; i++) {
				
				if (textFields [i].name == "Legend" + index) {
					textFields [i].gameObject.SetActive (true);
				} else {
					textFields [i].gameObject.SetActive (false);
				}
			}

		}

		virtual public void ShowChildButton() {
			
			buttonChild.gameObject.SetActive (true);
		}
	}
}
