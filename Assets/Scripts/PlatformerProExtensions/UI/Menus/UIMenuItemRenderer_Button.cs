﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using PlatformerPro.Extras;

namespace PlatformerPro.Extensions {
	
	public class UIMenuItemRenderer_Button : UIMenuItemRenderer_Image, IPointerEnterHandler {

		/// <summary>
		/// The color of the text when selected.
		/// </summary>
		[Tooltip("The new image when the button is selected")]
		[SerializeField]
		protected Sprite selectedImage;

		/// <summary>
		/// Force render a image with the same UIMenuItem.title inspector
		/// </summary>
		[Tooltip("Force render a image with the same UIMenuItem.title inspector")]
		[SerializeField]
		protected bool imageWithMenuTitle;

		/// <summary>
		/// Cache reference to Unity button component
		/// </summary>
		protected Button btn;

		/// <summary>
		/// Cache reference to the defaultImage (optional)
		/// </summary>
		protected Sprite defaultImage;

		public void OnPointerEnter(PointerEventData data) {

			var buttons = this.gameObject.transform.parent.GetComponentsInChildren<Button>();

			if (buttons.Length > 0) {
				
				foreach (var item in buttons) {
					if (item.name != this.name) {
						var itemRenderer = (IMenuItemRenderer) item.GetComponent (typeof(IMenuItemRenderer));
						if (itemRenderer != null) {
							itemRenderer.Deselect ();
						}
					}
				}
			}
		}


		/// <summary>
		/// Update to show the specified menu item.
		/// </summary>
		/// <param name="item">Item.</param>
		public override void InitMenuItem(UIMenuItem item)
		{
			btn = GetComponent<Button> ();
			if (btn == null) {
				Debug.LogError ("This gameObject needs a Button component");

			}else{
				btn.onClick.AddListener(() => {
					item.DoAction();
				});

			}

			if(textField == null) textField = GetComponentInChildren<Text> ();

			if (image == null && btn.image != null) {
				image = btn.image;

				base.InitMenuItem (item);
				image.color = Color.white;
			} else {
				menuItem = item;
				menu = (IMenu) item.GetComponentInParent (typeof(IMenu));
				textField.color = defaultColor;
				Refresh ();
			}

		}

		/// <summary>
		/// Force renderer to redraw with latest data.
		/// </summary>
		public override void Refresh()
		{

			if (imageWithMenuTitle) {
				
				textField.text = menuItem.ExtraInfo;
				if (image.mainTexture.name != menuItem.Title) {
					Sprite texture = (Sprite)Resources.Load (menuItem.Title, typeof(Sprite));
					image.sprite = texture;
				}
			} else {
				textField.text = menuItem.Title;
			}

		}
			

		/// <summary>
		/// Item was selected.
		/// </summary>
		public override void Select()
		{
			textField.color = selectedColor;
			if (btn.transition == Selectable.Transition.SpriteSwap) {

				if (btn.spriteState.highlightedSprite) {
					defaultImage = btn.image.sprite;
					btn.image.sprite = btn.spriteState.highlightedSprite;
				} else {
					btn.image.sprite = selectedImage;
				}

			}
		}

		// <summary>
		/// Item was deselected.
		/// </summary>
		public override void Deselect()
		{
			textField.color = defaultColor;

			if (btn.transition == Selectable.Transition.SpriteSwap) {
				
				image.sprite = defaultImage ? defaultImage : btn.image.sprite;
			}
		}
			
	}
}
