﻿using UnityEngine;
using System.Collections;
using System;
using PlatformerPro;
using TweenMode = PlatformerPro.Tween.TweenMode;

namespace PlatformerPro.Extensions { 
	
	public class SmoothCenteringTeleportCamera : SmoothCenteringZoneCamera {

		/// <summary>
		/// Moves a character player to another zone with a gameObject that has a Respawn component.
		/// </summary>
		[SerializeField]
		[Tooltip("Moves the character to a first active Respawn point when Zone is changed. Use LevelManager class to do this")]
		protected bool teleportCharacter;

		/// <summary>
		/// Replaces the original camera y-axis
		/// </summary>
		[SerializeField]
		[Tooltip("Replaces the original camera x and y-axis")]
		protected Vector2 respawnCamPosition = Vector2.zero;

		/// <summary>
		/// The animator reference to play
		/// when player is moved
		/// </summary>
		[SerializeField]
		[Tooltip("The animator controller to play a animation when player is moved")]
		protected Animator teleportAnimator;

		/// <summary>
		/// The type animation to play when teleport the player
		/// </summary>
		[SerializeField]
		[Tooltip("The type animation to play when teleport the player")]
		protected TeleportAnimationType animationType;

		/// <summary>
		/// Enable tween mode effect to move the camera?
		/// </summary>
		[SerializeField]
		[Tooltip("Enable tween mode effect to move the camera?")]
		protected bool enableTween = true;

		/// <summary>
		/// Rotate the camera when change to a new zone?
		/// </summary>
		[SerializeField]
		[Tooltip("Rotate the camera when change to a new zone?")]
		protected bool rotateNewZone = true;

		/// <summary>
		/// The cached zone origin. Stored before
		/// change to another zone
		/// </summary>
		protected CameraZoneExtended originZone;

		/// <summary>
		/// The cached gameObject of a zone origin. Stored before
		/// change to another zone, and created dinamically to the
		/// Unity game objects hierarchy
		/// </summary>
		protected GameObject originCachedZone;

		/// <summary>
		/// The cached player origin rotation coordinates. Stored before
		/// change to another zone
		/// </summary>
		protected Quaternion originPlayerRotation;

		/// <summary>
		/// Back to the origin zone?
		/// </summary>
		protected bool goBack = false;

		/// <summary>
		/// Gets a value indicating whether this instance has origin zone.
		/// </summary>
		/// <value><c>true</c> if this instance has origin zone; otherwise, <c>false</c>.</value>
		public bool HasOriginZone {
			get {
				return originZone != null ? true : false ;
			}
		}

		/// <summary>
		/// Additionally to the base method, add a Respawned 
		/// event to a level when Init() is called
		/// </summary>
		override public void Init() 
		{
			base.Init ();

			if (character != null) {
				LevelManager.Instance.Respawned += HandleLevelRespawned;
			}
		}

		/// <summary>
		/// Changes the zone by smoothly animating to the new zone.
		/// </summary>
		/// <param name="newZone">The zone to move to.</param>
		override public void ChangeZone(CameraZone newZone)
		{

			if (!goBack) {
				
				originCachedZone = new GameObject ();
				originCachedZone.name = "originCameraZone";

				CameraZoneExtended cameraZoneComp = originCachedZone.AddComponent<CameraZoneExtended> ();
				cameraZoneComp.fieldOfView = myCamera.fieldOfView;
				cameraZoneComp.width = newZone.width;
				cameraZoneComp.height = newZone.height;

				originCachedZone.transform.position = myCamera.transform.position;
				originCachedZone.transform.rotation = myCamera.transform.rotation;


				originZone = originCachedZone.GetComponent<CameraZoneExtended> ();
			}

			// TODO: Enable tween camera effect by a specific CameraZone
			if (enableTween || (newZone is CameraZoneExtended && ((CameraZoneExtended)newZone).enableTween) ) {

				if (newZone is CameraZoneExtended) {
					var movementMode = ((CameraZoneExtended)newZone).tweenMode;

					if (movementMode != tweenMode) {
						tweenMode = movementMode;
					}
				}

				base.ChangeZone (newZone);

			} else {
				movingInX = false;
				movingInY = false;
				distanceMovedLastFrameX = 0.0f;
				distanceMovedLastFrameY = 0.0f;
				if (freezePlayerDuringTransition && character != null)
					character.enabled = false;

				myCamera.transform.position = newZone.transform.position + new Vector3 (0, 0, newZone.cameraZOffset);

				if (rotateNewZone) {
					myCamera.transform.rotation = newZone.transform.rotation;

					float angle = Quaternion.Angle (character.transform.rotation, myCamera.transform.rotation);
					if (angle > 0) {
						originPlayerRotation = character.gameObject.transform.rotation;

						if (character.Input is StandardInput) {
							((StandardInput)character.Input).reverseHorizontalAxis = true;
						}
					}

				}

				if (newZone is CameraZoneExtended) {
					float newFieldOfView = ((CameraZoneExtended)newZone).fieldOfView;
					
					if (newFieldOfView != myCamera.fieldOfView) {
						myCamera.fieldOfView = newFieldOfView;
					}
				}
					
				targetZone = newZone;

				ZoneHasBeenChanged (transform, newZone.CameraPosition);
			}

		}

		/// <summary>
		/// Called when the zones the been changed.
		/// </summary>
		/// <param name="t"> Target that will be moved (e.g the Camera) </param>
		/// <param name="p"> The position destination for the new area </param>
		override public void ZoneHasBeenChanged(Transform t, Vector3 p)
		{
			base.ZoneHasBeenChanged (t, p);

			if (teleportCharacter) {

				if (teleportAnimator != null) {
					StartCoroutine(TeleportAnimation (() => LevelManager.Instance.Respawn (character)));
				} else {
					LevelManager.Instance.Respawn (character);
				}

				movingInX = false;
				movingInY = false;

				xAxis.moveOnAxis = false;
				yAxis.moveOnAxis = false;
				currentZone = null;

			}
			isInTransition = false;
			
		}

		/// <summary>
		/// Backs to origin zone.
		/// </summary>
		public virtual void BackToOriginZone() {
			
			if (originZone != null) {

				if (character.Input is StandardInput) {
					((StandardInput)character.Input).reverseHorizontalAxis = false;
				}

				goBack = true;
				ChangeZone (originZone);
				originZone = null;

				Destroy (originCachedZone);
				originCachedZone = null;
				goBack = false; 
			}
		}

		/// <summary>
		/// Handles the respawned event
		/// Now, moves change the Y axis camera to a custom position
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">Even details.</param>
		override protected void HandleRespawned (object sender, CharacterEventArgs e)
		{
			float newY = character.transform.position.y - transform.position.y;
			float newX = character.transform.position.x - transform.position.x;
			if (respawnCamPosition.y > 0) {	
				newY = character.transform.position.y - respawnCamPosition.y;
			}

			if (respawnCamPosition.x > 0) {	
				newX = character.transform.position.x - respawnCamPosition.x;
			}


			transform.Translate(newX, newY, 0);
		}

		/// <summary>
		/// Check if a respawn point is a EnterableDoor
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		virtual protected void HandleLevelRespawned (object sender, SceneEventArgs e)
		{
			RespawnPoint[] respawnPoints = GameObject.FindObjectsOfType<RespawnPoint> ();
			foreach (var r in respawnPoints) {
				if (r.identifier == e.RespawnPoint) {
					var door = r.GetComponent<EnterableDoorWithIcon> ();
					if (door != null) {
						door.isEntered = false;
					}
				}
			}
		}

		/// <summary>
		/// Teleports the animation.
		/// </summary>
		/// <returns>The animation.</returns>
		/// <param name="fn">Fn.</param>

		//TODO: Change the fixed string "FADE" to get a choice on inspector 
		public virtual IEnumerator TeleportAnimation(Action fn){

			if (animationType != TeleportAnimationType.NONE) {
				teleportAnimator.Play ("FADE");
			}

			AnimatorStateInfo stateInfo = teleportAnimator.GetCurrentAnimatorStateInfo(0);
			yield return new WaitForSeconds (stateInfo.length);

			fn ();

			teleportAnimator.Play ("NO_FADE");
		}

	}
}
