﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// The platformer customs (Mecanics, Dymanics...)
/// </summary>
namespace PlatformerPro.Extensions {
	
	/// <summary>
	/// Mecanic - Move the player and a camera to a new place through the scene
	/// </summary>
	public class Teleporter : MonoBehaviour {
		
		/// <summary>
		/// The player reference data
		/// </summary>
		[SerializeField]
		[Tooltip("The player reference data")]
		protected PlayerRef player;

		/// <summary>
		/// The camera reference data
		/// </summary>
		[SerializeField]
		[Tooltip("The camera reference data")]
		protected CameraRef cam;

		/// <summary>
		/// The fade animator reference to play
		/// when player is moved
		/// </summary>
		[SerializeField]
		[Tooltip("The fade animator controller to play a animation when player is moved")]
		protected Animator fadeAnimator;

		/// <summary>
		/// Property to get the origin player position coordinates from
		/// another scripts
		/// </summary>
		/// <value>The origin player position Vector3 instance.</value>
		public Vector3 OriginPlayer { get; set; }


		#region unity hooks

		void Awake() {
			
			cam.Init(player);

		}

		void OnTriggerEnter2D(Collider2D other) {
			
			if (other.tag == player.Tag) {
				
				StartCoroutine(this.Fade( () => {
					this.Move();
				}));

			}
		}

		#endregion

		/// <summary>
		/// Move the player and the camera to a new destination
		/// </summary>
		/// <param name="playerDest">The new <c>Vector3</c> player destination</param>
		/// <param name="camDest">The new <c>Vector3</c> camera destination</param>
		public void Move(Vector3 playerDest, Vector3 camDest) {
			
			this.OriginPlayer = player.Transform.position;
			player.Transform.position = playerDest;

			cam.MoveScript.Zoom(camDest);
		}

		/// <summary>
		/// Move the player and the camera to a new destination
		/// defined by <see cref="playerDestination"/>
		/// and <see cref="camDestination"/> properties
		/// </summary>
		public void Move() {
			
			this.OriginPlayer = player.Transform.position;
			player.Transform.position = player.Destination;

			cam.MoveScript.Zoom(cam.Destination);
		}

		/// <summary>
		/// <para>
		/// Play the "fade" animation.
		/// </para>
		/// Requires a "FadeIn" Unity animator param
		/// </summary>
		public IEnumerator Fade(Action callback) {

			fadeAnimator.Play ("FADE");

			AnimatorStateInfo stateInfo = fadeAnimator.GetCurrentAnimatorStateInfo(0);
			yield return new WaitForSeconds (stateInfo.length);

			fadeAnimator.Play ("NO_FADE");

			callback ();
		}
	}
}
