﻿using UnityEngine;
using System.Collections;
using PlatformerPro;
using PlatformerPro.Tween;

namespace PlatformerPro.Extensions {
	
	public class CameraZoneExtended : CameraZone {

		/// <summary>
		/// New value for camera field of view to the zone.
		/// </summary>
		[Header("Extended Settings")]
		[SerializeField]
		[Range(1, 179)]
		[Tooltip("New value for camera field of view to the zone")]
		public float fieldOfView;

		/// <summary>
		/// Enable/Disable Tween effect when change zone
		/// </summary>
		[SerializeField]
		[Tooltip("Use tween effect on change zone?")]
		public bool enableTween;

		/// <summary>
		/// How to move to the new camera zone (by each CameraZone).
		/// </summary>
		[SerializeField]
		[Tooltip("How to move to the new camera zone (by each CameraZone)")]
		public TweenMode tweenMode;

	}
}
