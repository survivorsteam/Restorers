﻿using UnityEngine;
using System.Collections;
using PlatformerPro;

namespace PlatformerPro.Extensions {
	
	/// <summary>
	/// Trigger to controls Start/Stop camera Y-axi character movement.
	/// </summary>
	[RequireComponent(typeof(BoxCollider2D), typeof(Rigidbody2D))]
	public class MoveAxisArea : MonoBehaviour {

		#region members

		[SerializeField]
		[Tooltip("The PlatFormerPro smooth center follow camera script")]
		protected SmoothCenteringZoneCamera smoothCamera;

		[SerializeField]
		[Tooltip("The character gameObject reference to verify trigger")]
		protected GameObject player;

		/// <summary>
		/// Fire all trigger once only and disable required collider?
		/// </summary>
		[Tooltip ("Fire all triggers once only then disable the collider component?")]
		[SerializeField]
		protected bool oneShot = false;

		/// <summary>
		/// Verify the player height?
		/// </summary>
		[Tooltip ("Verify the player heigth?")]
		[SerializeField]
		protected bool checkHeight = false;

		[SerializeField]
		[Tooltip("The camera will continue follow the player axis ON ENTER?")]
		protected AreaAxisSettings triggerEnter;

		[SerializeField]
		[Tooltip("The camera will continue follow the player axis ON STAY?")]
		protected AreaStayAxis triggerStay;

		[SerializeField]
		[Tooltip("The camera will continue follow the player axis ON EXIT?")]
		protected AreaAxisSettings triggerExit;

		/// <summary>
		/// The cached collider reference
		/// </summary>
		protected BoxCollider2D colliderTrigger;

		/// <summary>
		/// Skip all triggers?
		/// </summary>
		protected bool skipTriggers = false;

		#endregion

		#region unity hooks

		// Use this for run before components is enable
		void Awake () {
			colliderTrigger = GetComponent<BoxCollider2D> ();
			colliderTrigger.isTrigger = true;

			GetComponent<Rigidbody2D> ().isKinematic = true;
		}

			
		void OnTriggerEnter2D(Collider2D other){
			if (checkHeight) {
				if (player.transform.position.y <= transform.position.y) {
					skipTriggers = true;
					return;
				} else {
					skipTriggers = false;
				}
			}

			if (other.CompareTag (player.tag) && !skipTriggers) {
				ToggleMoveAxis ('x',triggerEnter.moveInX);
				ToggleMoveAxis ('y',triggerEnter.moveInY);
			}
		}

		void OnTriggerStay2D(Collider2D other){

			if (other.CompareTag (player.tag) && !skipTriggers && !triggerStay.skip) {
				ToggleMoveAxis ('x',triggerStay.moveInX);
				ToggleMoveAxis ('y',triggerStay.moveInY);
			}

		}

		void OnTriggerExit2D(Collider2D other){
			
			if (other.CompareTag (player.tag) && !skipTriggers) {
				ToggleMoveAxis ('x',triggerExit.moveInX);
				ToggleMoveAxis ('y',triggerExit.moveInY);

				if (oneShot) {
					colliderTrigger.enabled = false;
				}
			}

		}

		#endregion

		/// <summary>
		/// Start/Stop movement on specific axis
		/// </summary>
		/// <param name="axis">The axis coordinate to continue moving.</param>
		/// <param name="continueMoving">If set to <c>true</c> continue moving.</param>
		public virtual void ToggleMoveAxis(char axis = 'x',bool continueMoving = true) {

			if (axis == 'x') {
				smoothCamera.xAxis.moveOnAxis = continueMoving;
			}

			if (axis == 'y') {
				smoothCamera.yAxis.moveOnAxis = continueMoving;
			}
		}
			
	}

	[System.Serializable]
	public class AreaAxisSettings {

		[Tooltip("The camera will continue follow the player in X-axis? ")]
		public bool moveInX = true;

		[Tooltip("The camera will continue follow the player in Y-axis? ")]
		public bool moveInY= true;
	}

	[System.Serializable]
	public class AreaStayAxis {

		[Tooltip("The camera will continue follow the player in X-axis? ")]
		public bool moveInX = false;

		[Tooltip("The camera will continue follow the player in Y-axis? ")]
		public bool moveInY= false;

		[Tooltip("Skip verify this axis to move? ")]
		public bool skip = true;
	}
}
