﻿using UnityEngine;
using System.Collections;

public interface ICameraMove {

	void Zoom(Vector3 position);
}
