﻿using UnityEngine;
using System.Collections;

public class Interaction : MonoBehaviour {

	/// <summary>
	/// The player reference data
	/// </summary>
	[SerializeField]
	[Tooltip("The player reference data")]
	protected GameObject player;

	/// <summary>
	/// The player reference data
	/// </summary>
	[SerializeField]
	[Tooltip("The target interact game object")]
	protected GameObject interactElement;

	/// <summary>
	/// The player reference data
	/// </summary>
	[SerializeField]
	[Tooltip("The user interact button")]
	protected GameObject icon;

	// Use this for initialization
	void OnCollisionEnter2D (Collision2D other) {
		
		if (player.CompareTag (other.gameObject.tag)) {
//			Animator interactAnim = interactElement.GetComponent<Animator> ();
//			interactAnim.SetBool ("interact", true);
			icon.SetActive (true);
		}
	}
	
	// Update is called once per frame
	void OnCollisionExit2D(Collision2D other) {
		if (player.CompareTag (other.gameObject.tag)) {
			
			icon.SetActive (false);
		}
	}
}
