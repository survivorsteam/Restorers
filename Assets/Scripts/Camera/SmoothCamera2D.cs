﻿using UnityEngine;
using System.Collections;
using CameraMovement;

public partial class SmoothCamera2D : MonoBehaviour, ICameraMove {

	public void Zoom(Vector3 position){
		transform.position = position;
	}
}
