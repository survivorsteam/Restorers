﻿using UnityEngine;
using System.Collections;
using CameraMovement;

namespace CameraMovement
{
	/// <summary>
	/// Stops the target camera following when 
	/// it enter on this area.
	/// </summary>
	public class StopCamArea : MonoBehaviour {

		/// <summary>
		/// The followed target gameobject (example: A player object)
		/// </summary>
		[SerializeField]
		[Tooltip("The followed target gameobject (example: A player object)")]
		protected GameObject followedObject;

		/// <summary>
		/// The smooth follow camera reference
		/// </summary>
		[Tooltip("The smooth follow camera reference")]
		[SerializeField]
		protected Camera cam;

		/// <summary>
		/// <para>
		/// The smooth script of the camera.
		/// </para>
		/// This is stored internally from <see cref="StopCamArea#Awake"/>method
		/// </summary>
		private CameraMovement.SmoothCamera2D smooth;

		#region unity hooks

		void Awake() {
			smooth = cam.GetComponent<CameraMovement.SmoothCamera2D>();

			if (!smooth) {
				throw new MissingComponentException ("The camera gameobject needs have a 'SmoothCamera2D' script attached");
			}
		}

		void OnTriggerStay2D(Collider2D other) {
			
			if (other.CompareTag(followedObject.tag)) {

				var collider = GetComponent<Collider2D>();
				if (collider.isTrigger) {
					
					smooth.Stop();
				} else {
					Debug.LogError ("The stoppable camera area needs be a TRIGGER");
				}
			}
		}

		void OnTriggerExit2D(Collider2D other) {
			
			if (other.CompareTag(followedObject.tag)) {
				
				smooth.Continue();
			}
		}

		#endregion
	}
}
