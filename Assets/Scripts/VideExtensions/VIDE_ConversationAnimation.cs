﻿using UnityEngine;
using System.Collections;

/// <summary>
/// VID e conversation animation.
/// </summary>
public class VIDE_ConversationAnimation : VIDE_ConversationEvents {

	[SerializeField]
	protected Animator animator;

	[SerializeField]
	protected string animationName;

	[SerializeField]
	protected int commentIndex = 0;

	[SerializeField]
	protected bool wait = true;


	protected virtual void OnEnable() {
		if (conversation == null) {
			Debug.LogWarning ("The ConversationAnimation needs at least a UIConversation component");
		} else {

			conversation.CommentFinished += HandleCommentShow;
		}

	}

	protected virtual void OnDisale() {

		if (conversation != null) {

			conversation.CommentFinished -= HandleCommentShow;
		}
	}
	
	public override void HandleCommentShow(object sender, CommentEventArgs commentArgs){
		
		if (commentArgs.Type == CommentOfType.ITEM 
			&& commentArgs.Index == commentIndex
			&& commentArgs.NodeData.npcCommentIndex == (commentArgs.NodeData.npcComment.Length -1)) {

			if (wait) {
				StartCoroutine (
					WaitAnimation (animator, animationName)
				);
			} else {
				animator.Play (animationName);
			}
		}
	}
}
