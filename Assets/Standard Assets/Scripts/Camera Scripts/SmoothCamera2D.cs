﻿using UnityEngine;
using System.Collections;

/// <summary>
/// <para>
/// Follow the target with Smooth X-axis like a 2D camera. 
/// This script is simple and works to platformer/side-scrolling games, 3D or 2D.
/// </para>
/// <para>
/// 
/// </para>
/// <para>
/// Based on script written by Scott Kovacs via UnityAnswers.com; Oct 5th 2010
/// </para>
/// <para>
/// http://answers.unity3d.com/questions/29183/2d-camera-smooth-follow.html
/// </para>
/// </summary>
/// 

namespace CameraMovement
{
		
	[RequireComponent(typeof(Camera)), DisallowMultipleComponent]
	public class SmoothCamera2D : MonoBehaviour {
		
		#region public params

		/// <summary>
		/// The target that camera will follow
		/// </summary>
		[SerializeField]
		[Tooltip("The target that camera will follow")]
		protected Transform target;

		/// <summary>
		/// Seconds to damp a smooth camera
		/// </summary>
		[SerializeField]
		[Tooltip("Seconds to damp a smooth camera")]
		protected float dampTime = 0.15f;

		/// <summary>
		/// Replaces the original camera y-axis
		/// </summary>
		[SerializeField]
		[Tooltip("Replaces the original camera y-axis")]
		protected float positionY = 0.0f;

		/// <summary>
		/// Replaces the original camera y-axis
		/// </summary>
		[SerializeField]
		[Tooltip("How distant from the player based on the world, to set the initial camera position")]
		protected Vector2 viewPortToWorld = new Vector2(0.1f, 0.1f);


		#endregion

		#region internal members

		/// <summary>
		/// The velocity to the smooth. Used internally
		/// </summary>
		protected Vector3 velocity = Vector3.zero;

		/// <summary>
		/// Camera component reference
		/// </summary>
		protected Camera cam;

		private bool keepFollow = true;

		#endregion

		#region unity hooks

		void Start()
		{
			if (!target) {
				throw new MissingReferenceException ("The 'target' param needs be defined to camera follow");
			}
			cam = GetComponent<Camera>();

		}

		// Update is called once per frame
		void FixedUpdate()
		{

			if (target && keepFollow)
			{
				FollowSmooth();

			}

		}

		#endregion

		/// <summary>
		/// Follows the target smoothly on each fixed frame.
		/// </summary>
		/// <see cref="target"/>
		public void FollowSmooth()
		{

			Vector3 point = cam.WorldToViewportPoint(target.position);
			Vector3 delta = target.position - cam.ViewportToWorldPoint(new Vector3(viewPortToWorld.x, viewPortToWorld.y, point.z));
			Vector3 destination = transform.position + delta;

			// Set this to the Y position you want to the camera
			if (positionY > 0) {
				destination.y = positionY;	
			} else {
				destination.y = transform.position.y;
			}


			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
		}

		/// <summary>
		/// Stop follow the target when wraps a "stoppable area"
		/// </summary>
		/// <see cref="keepFollow"/>
		public void Stop()
		{
			if (keepFollow) {
				keepFollow = false;
			}
				
		}

		/// <summary>
		/// Keep following the target
		/// </summary>
		/// <see cref="keepFollow"/>
		public void Continue() 
		{
			if (!keepFollow) {
				keepFollow = true;
			}
		}
	}
}