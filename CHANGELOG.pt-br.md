# Change Log
Todas as alterações do jogo **Restauradores**, desde a versão beta `v0.1`

## [0.1] - 01/12/2016
### Added
- Acesso a sala acima da NPC Clarice que deve ser aberta com uma Chave
- Acesso a a primeira escada
- Animações do player diferentes por arma (**soco ou faca**) 


### Changed
- Modificada as imagens de UI do inventário/craft
- Não remover uma chave do inventário ao clicar com o botão direito

### Fixed
- Bug de NullPointer ao confirmar criação do item no **craft**
- Chave era entregue pela NPC Clarice várias vezes

## [0.2] - 13/12/2016
### Added
- Efeitos sonoros: Valkiria e Cachorro
- Efeitos sonoros: Inventário -> ao arrastar itens e ao realizar um craft 
- Trilha sonora: Suspense ao entrar na fábrica e de combate

### Changed
- Refatorações de algumas classes C# (eventos do inventário, por exemplo)

## [0.3] - 06/01/2017
### Added
- Trilha sonora da cutscene inicial (após o menu)

### Fixed
- Trilha da fábrica ao mudar de THRILLER para FIGHT parava de tocar

### Changed
- Trilha de tensão agora também é executada no combate com **Ben**
- Refatorações na classe `SceneLoader` para parar a trilha da cutscene.