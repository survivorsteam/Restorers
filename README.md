# Restorers - Unity3D platformer game

This is a 2.5D game project built in [Unity3D](https://unity3d.com/pt) game engine free version.

### UNITY VERSION: **`5.4.3f`**

![restorers-prototype.png](https://gitlab.com/survivorsteam/Restorers/raw/master/ImagesPreview/restorers-screenshot.png)

### What is this repository for?

* Share the code to play test and feature improvements with the group **[SurvivorsTeam](https://gitlab.com/groups/survivorsteam)**

* Realease downloads to teachers and another students tests and feedbacks!

### Dependencies

This project contains all assets dependencies for the time being (scripts, sprites, external plugins...). This increases the size of this repository and will be removed in alpha or beta versions

 **Asset Store:**

1. [Unity Standard Assets](https://www.assetstore.unity3d.com/en/#!/content/32351)
> `CrossPlaformInput`, and `DustStormparticle` scripts

2. **Platformer PRO - LE** (Removed from unity asset store)
> All package contents for while

3. [VIDE Dialogues](https://www.assetstore.unity3d.com/en/#!/content/69932) (Free asset for conversations)
> All package contents for while

4. [FMOD](http://www.fmod.org/download/) (Audio middleware)
> All package contents for while

### Getting Started

1) Clone this repository into a directory of your choice, using the command line below:

```ssh
git clone git@gitlab.com:survivorsteam/Restorers.git
```

Or download a zip file from the dowload project button: ![Download](https://gitlab.com/survivorsteam/Restorers/raw/master/ImagesPreview/download-project.png)

2) Clone the latest **VIDE** asset fork with custom changes from original version, into `Assets/VIDE` folder

```ssh
cd Restorers
git clone git@gitlab.com:mfdeveloper/unity-vide-dialogue.git Assets/VIDE
``` 

3) Open **Unity3D** software, click on `File => Open Project` (Or click on `Open` main screen button) and select the `Restorers` folder
> Extracted downloaded zip file or cloned repo.

4) If you don't see anything into the `Scene` Unity tab, open the file `Scenes => Menu.unity` or `Scenes => Fabric.unity` (to direct access to the main scene) of `Assets` project folder

5) Click on `Play` button on top of Unity editor

6) Enjoy, test and report errors for us :)
